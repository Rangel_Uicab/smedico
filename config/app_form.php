<?php
/*return [
    'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>',
    'inputContainer' => '<div class="form-group">{{content}}</div>',
    'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
    'label' => '<label{{attrs}}>{{text}}</label>',
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
    'checkboxFormGroup' => '<div class="checkbox">{{label}}</div>',
    'checkboxWrapper' => '<div>{{label}}</div>',
    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
    'selectMultiple' => '<div class="btn-group bootstrap-select form-control"><select class="selectpicker" name="{{name}}[]" multiple data-live-search="true" {{attrs}}>{{content}}</select></div>',
];*/

return [
    'input'=>'<input class="form-control" type="{{type}}" name="{{name}}" {{attrs}}/>',
    'inputContainer'=>'<div class="form-group form-material"><div class="fg-line" >{{content}}</div></div>',
    'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
    'label' => '<label{{attrs}}>{{text}}</label>',
    
   /* <div class="checkbox checkbox-circle checkbox-info peers ai-c mB-15">
                    <input type="checkbox" id="inputCall1" name="inputCheckboxesCall" class="peer">
                    <label for="inputCall1" class="peers peer-greed js-sb ai-c">
                        <span class="peer peer-greed">Call John for Dinner</span>
                    </label>
                </div>
*/
    /*'checkbox' => '
                    <div class="checkbox checkbox-circle checkbox-info peers ai-c mB-15">
                        <input type="checkbox" name="{{name}}" class="peer">
                        
                        <label for="{{name}}" class="peers peer-greed js-sb ai-c">
                            <span class="peer peer-greed">{{label}}</span>
                        </label>
                    </div>
                  ',*/
    //'checkboxFormGroup' => '<div class="form-group form-material">{{label}}</div>',
    //'checkboxWrapper' => '<div>{{label}}</div>',
//    'checkbox' => '<div class="checkbox-custom checkbox-default"><input type="checkbox" autocomplete="off" name="{{name}}"/></div>',
    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
    'selectMultiple' => '<div class="form-group"><select class="form-control" name="{{name}}[]" multiple {{attrs}}>{{content}}</select></div>',
    'radioContainer'=>'<div class="form-group form-material"><div class="fg-line" >{{content}}</div></div>'
]
?>
