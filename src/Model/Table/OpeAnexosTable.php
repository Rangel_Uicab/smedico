<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OpeAnexos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OpeServicios
 * @property \Cake\ORM\Association\BelongsTo $CoUsuarios
 *
 * @method \App\Model\Entity\OpeAnexo get($primaryKey, $options = [])
 * @method \App\Model\Entity\OpeAnexo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OpeAnexo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OpeAnexo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpeAnexo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OpeAnexo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OpeAnexo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OpeAnexosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ope_anexos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
		
		//******UPLOAD*********
		
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'img' => 
            [
                'fields' => 
                [
                    'dir' => 'dir',
                    'size' => 'size',
                    'type' => 'type'
                ],
//                 'path' => 'webroot{DS}img{DS}anexos{DS}',
                 'path' => 'src{DS}files{DS}',
                         // A pathProcessor handles both returning the basepath
        // as well as what the initial filename should be set to
        //'pathProcessor' => 'Josegonzalez\Upload\File\Path\DefaultProcessor',
        // Allows you to create new files from the original source,
        // or possibly even modify/remove the original source file
        // from the upload process
//        'transformer' => 'Josegonzalez\Upload\File\Transformer\DefaultTransformer',
        // Handles writing a file to disk... or S3... or Dropbox... or FTP... or /dev/null
        'writer' => 'Josegonzalez\Upload\File\Writer\DefaultWriter',
                'nameCallback' => function ($data, $settings) 
                {
                    return strtolower($data['name']);
                },
                //'transformer' =>  function ($table, $entity, $data, $field, $settings) 
//                {
//                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
//
//                    // Store the thumbnail in a temporary file
//                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
//
//                    // Use the Imagine library to DO THE THING
//                    $size = new \Imagine\Image\Box(40, 40);
//                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
//                    $imagine = new \Imagine\Gd\Imagine();
//
//                    // Save that modified file to our temp file
//                    $imagine->open($data['tmp_name'])
//                        ->thumbnail($size, $mode)
//                        ->save($tmp);
//
//                    // Now return the original *and* the thumbnail
//                    return [
//                        $data['tmp_name'] => $data['name'],
//                        $tmp => 'thumbnail-' . $data['name'],
//                    ];
//                },
                'deleteCallback' => function ($path, $entity, $field, $settings) 
                {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        $path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            

        ]);
		//******UPLOAD*********
		
		
        $this->addBehavior('Timestamp');

        $this->belongsTo('OpeServicios', [
            'foreignKey' => 'ope_servicio_id',
            'joinType' => 'INNER'
        ]); 
        $this->belongsTo('CatTiposOficios', [
            'foreignKey' => 'cat_tipos_oficio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('img');

        $validator
            ->allowEmpty('dir');

        $validator
            ->integer('size')
            ->allowEmpty('size');

        $validator
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ope_servicio_id'], 'OpeServicios'));
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));

        return $rules;
    }
}
