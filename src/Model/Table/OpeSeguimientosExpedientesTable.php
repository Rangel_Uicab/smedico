<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * OpeSeguimientosExpedientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OpeExpedientes
 * @property \Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \Cake\ORM\Association\BelongsTo $CatEstatus
 *
 * @method \App\Model\Entity\OpeSeguimientosExpediente get($primaryKey, $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OpeSeguimientosExpediente findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OpeSeguimientosExpedientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ope_seguimientos_expedientes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OpeExpedientes', [
            'foreignKey' => 'ope_expediente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('CatAreas', [
            'foreignKey' => 'cat_area_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatEstatus', [
            'foreignKey' => 'cat_estatu_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('observacion', 'create')
            ->notEmpty('observacion');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ope_expediente_id'], 'OpeExpedientes'));
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));
        $rules->add($rules->existsIn(['cat_estatu_id'], 'CatEstatus'));

        return $rules;
    }
    
    public function beforeSave(Event $event, Entity $entity)
    {
     $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
     $modificadas ='aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
     
     $entity->observacion = mb_strtoupper(strtr(utf8_decode($entity->observacion), utf8_decode($originales), $modificadas) ,'utf-8');      
    }
    
}
