<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CoPermisos Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $CoGrupos
 *
 * @method \App\Model\Entity\CoPermiso get($primaryKey, $options = [])
 * @method \App\Model\Entity\CoPermiso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CoPermiso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CoPermiso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CoPermiso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CoPermiso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CoPermiso findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CoPermisosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('co_permisos');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('CoGrupos', [
            'foreignKey' => 'co_permiso_id',
            'targetForeignKey' => 'co_grupo_id',
            'joinTable' => 'co_grupos_co_permisos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('descripcion');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
