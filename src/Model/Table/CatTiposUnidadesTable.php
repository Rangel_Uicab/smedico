<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatTiposUnidades Model
 *
 * @property \Cake\ORM\Association\HasMany $CatUnidades
 *
 * @method \App\Model\Entity\CatTiposUnidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatTiposUnidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatTiposUnidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposUnidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatTiposUnidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposUnidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposUnidade findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatTiposUnidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_tipos_unidades');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('CatUnidades', [
            'foreignKey' => 'cat_tipos_unidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
