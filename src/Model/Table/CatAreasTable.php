<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatAreas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \Cake\ORM\Association\HasMany $CoUsuarios
 *
 * @method \App\Model\Entity\CatArea get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatArea newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatArea[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatArea|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatArea patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatArea[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatArea findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatAreasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_areas');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CatUnidades', [
            'foreignKey' => 'cat_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CoUsuarios', [
            'foreignKey' => 'cat_area_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('descripcion');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_unidade_id'], 'CatUnidades'));

        return $rules;
    }
}
