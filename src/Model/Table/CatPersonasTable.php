<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * CatPersonas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \Cake\ORM\Association\BelongsTo $CatMunicipios
 * @property \Cake\ORM\Association\BelongsTo $CatLocalidades
 * @property \Cake\ORM\Association\HasMany $OpeExpedientes
 *
 * @method \App\Model\Entity\CatPersona get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatPersona newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatPersona[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatPersona|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatPersona patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatPersona[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatPersona findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatPersonasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_personas');
        $this->setDisplayField('nombre_completo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatUnidades', [
            'foreignKey' => 'cat_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OpeExpedientes', [
            'foreignKey' => 'cat_persona_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        $validator
            ->requirePresence('paterno', 'create')
            ->notEmpty('paterno');

        $validator
            ->requirePresence('materno', 'create')
            ->notEmpty('materno');

        $validator
            ->requirePresence('sexo', 'create')
            ->notEmpty('sexo');

        $validator
            ->allowEmpty('num_expediente');

        $validator
            ->allowEmpty('num_afiliacion');

        $validator
            ->date('fecha_nacimiento')
            ->allowEmpty('fecha_nacimiento');

        $validator
            ->allowEmpty('tel_movil');

        $validator
            ->allowEmpty('tel_casa');

        $validator
            ->allowEmpty('direccion');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));

        return $rules;
    }

    public function beforeSave(Event $event, Entity $entity)
    {
     $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
     $modificadas ='aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

     $entity->nombre = mb_strtoupper(strtr(utf8_decode($entity->nombre), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->paterno = mb_strtoupper(strtr(utf8_decode($entity->paterno), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->materno = mb_strtoupper(strtr(utf8_decode($entity->materno), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->vialidad = mb_strtoupper(strtr(utf8_decode($entity->vialidad), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->tipo_asentamiento = mb_strtoupper(strtr(utf8_decode($entity->tipo_asentamiento), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->nombre_asentamiento = mb_strtoupper(strtr(utf8_decode($entity->nombre_asentamiento), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->localidad = mb_strtoupper(strtr(utf8_decode($entity->localidad), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->municipio = mb_strtoupper(strtr(utf8_decode($entity->municipio), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->entidad_federativa = mb_strtoupper(strtr(utf8_decode($entity->entidad_federativa), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->entidad_federativa_nacimiento = mb_strtoupper(strtr(utf8_decode($entity->entidad_federativa_nacimiento), utf8_decode($originales), $modificadas) ,'utf-8');
     $entity->curp = mb_strtoupper(utf8_decode($entity->curp));
    }

}
