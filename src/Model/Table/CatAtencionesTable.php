<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatAtenciones Model
 *
 * @property \Cake\ORM\Association\HasMany $OpeExpedientes
 *
 * @method \App\Model\Entity\CatAtencione get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatAtencione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatAtencione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatAtencione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatAtencione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatAtencione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatAtencione findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatAtencionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_atenciones');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('OpeExpedientes', [
            'foreignKey' => 'cat_atencione_id'
        ]);
        $this->belongsToMany('CatUnidades', [
            'foreignKey' => 'cat_atencione_id',
            'targetForeignKey' => 'cat_unidade_id',
            'joinTable' => 'cat_unidades_cat_atenciones'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
