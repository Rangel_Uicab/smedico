<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatTiposAltas Model
 *
 * @property \Cake\ORM\Association\HasMany $OpeExpedientes
 *
 * @method \App\Model\Entity\CatTiposAlta get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatTiposAlta newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatTiposAlta[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposAlta|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatTiposAlta patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposAlta[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatTiposAlta findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatTiposAltasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_tipos_altas');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('OpeExpedientes', [
            'foreignKey' => 'cat_tipos_alta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->boolean('requiere_unidad')
            ->allowEmpty('requiere_unidad');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }
}
