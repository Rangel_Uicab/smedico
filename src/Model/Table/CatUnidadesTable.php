<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CatUnidades Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CatTiposUnidades
 * @property \Cake\ORM\Association\BelongsTo $CatLocalidades
 * @property \Cake\ORM\Association\BelongsTo $CatMunicipios
 * @property \Cake\ORM\Association\HasMany $CatAreas
 * @property \Cake\ORM\Association\HasMany $CoUsuarios
 *
 * @method \App\Model\Entity\CatUnidade get($primaryKey, $options = [])
 * @method \App\Model\Entity\CatUnidade newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CatUnidade[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CatUnidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CatUnidade findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CatUnidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cat_unidades');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CatTiposUnidades', [
            'foreignKey' => 'cat_tipos_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatLocalidades', [
            'foreignKey' => 'cat_localidade_id'
        ]);
        $this->belongsTo('CatMunicipios', [
            'foreignKey' => 'cat_municipio_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CatAreas', [
            'foreignKey' => 'cat_unidade_id'
        ]);
        $this->hasMany('CoUsuarios', [
            'foreignKey' => 'cat_unidade_id'
        ]);
        $this->belongsToMany('CatAtenciones', [
            'foreignKey' => 'cat_unidade_id',
            'targetForeignKey' => 'cat_atencione_id',
            'joinTable' => 'cat_unidades_cat_atenciones'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('nombre_corto', 'create')
            ->notEmpty('nombre_corto');

        $validator
            ->allowEmpty('latitud');

        $validator
            ->allowEmpty('longitud');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_tipos_unidade_id'], 'CatTiposUnidades'));
        $rules->add($rules->existsIn(['cat_localidade_id'], 'CatLocalidades'));
        $rules->add($rules->existsIn(['cat_municipio_id'], 'CatMunicipios'));

        return $rules;
    }
}
