<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CoGrupos Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $CoMenus
 * @property \Cake\ORM\Association\BelongsToMany $CoPermisos
 * @property \Cake\ORM\Association\BelongsToMany $CoUsuarios
 *
 * @method \App\Model\Entity\CoGrupo get($primaryKey, $options = [])
 * @method \App\Model\Entity\CoGrupo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CoGrupo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CoGrupo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CoGrupo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CoGrupo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CoGrupo findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CoGruposTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('co_grupos');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('CoMenus', [
            'foreignKey' => 'co_grupo_id',
            'targetForeignKey' => 'co_menu_id',
            'joinTable' => 'co_grupos_co_menus'
        ]);
        $this->belongsToMany('CoPermisos', [
            'foreignKey' => 'co_grupo_id',
            'targetForeignKey' => 'co_permiso_id',
            'joinTable' => 'co_grupos_co_permisos'
        ]);
        $this->belongsToMany('CoUsuarios', [
            'foreignKey' => 'co_grupo_id',
            'targetForeignKey' => 'co_usuario_id',
            'joinTable' => 'co_usuarios_co_grupos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('pagina_inicial', 'create')
            ->notEmpty('pagina_inicial');

        $validator
            ->boolean('activo')
            ->allowEmpty('activo');

        $validator
            ->boolean('notificacion')
            ->allowEmpty('notificacion');

        return $validator;
    }
}
