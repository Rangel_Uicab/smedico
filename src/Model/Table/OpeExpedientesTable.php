<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
/**
 * OpeExpedientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CoUsuarios
 * @property \Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \Cake\ORM\Association\BelongsTo $CatAreas
 * @property \Cake\ORM\Association\BelongsTo $CatPersonas
 * @property \Cake\ORM\Association\BelongsTo $CatAtenciones
 * @property \Cake\ORM\Association\BelongsTo $CatTiposAltas
 * @property \Cake\ORM\Association\BelongsTo $CatUnidades
 * @property \Cake\ORM\Association\HasMany $OpeSeguimientosExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente get($primaryKey, $options = [])
 * @method \App\Model\Entity\OpeExpediente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OpeExpediente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OpeExpediente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpeExpediente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OpeExpediente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OpeExpediente findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OpeExpedientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ope_expedientes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CoUsuarios', [
            'foreignKey' => 'co_usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatUnidades', [
            'foreignKey' => 'cat_unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatAreas', [
            'foreignKey' => 'cat_area_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatPersonas', [
            'foreignKey' => 'cat_persona_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatAtenciones', [
            'foreignKey' => 'cat_atencione_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CatTiposAltas', [
            'foreignKey' => 'cat_tipos_alta_id'
        ]);
        $this->belongsTo('CatUnidadesAltas', [
            'foreignKey' => 'cat_unidade_traslado_id',
            'className'=>'CatUnidades'
        ]); 
        $this->hasMany('OpeSeguimientosExpedientes', [
            'foreignKey' => 'ope_expediente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('motivo', 'create')
            ->notEmpty('motivo');

        $validator
            ->date('fecha')
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');

        $validator
            ->time('hora')
            ->requirePresence('hora', 'create')
            ->notEmpty('hora');

        $validator
            ->allowEmpty('observacion');

        $validator
            ->allowEmpty('observacion_alta');

        $validator
            ->date('fecha_alta')
            ->allowEmpty('fecha_alta');

        $validator
            ->time('hora_alta')
            ->allowEmpty('hora_alta');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['co_usuario_id'], 'CoUsuarios'));
        $rules->add($rules->existsIn(['cat_unidade_id'], 'CatUnidades'));
        $rules->add($rules->existsIn(['cat_area_id'], 'CatAreas'));
        $rules->add($rules->existsIn(['cat_persona_id'], 'CatPersonas'));
        $rules->add($rules->existsIn(['cat_atencione_id'], 'CatAtenciones'));
        $rules->add($rules->existsIn(['cat_tipos_alta_id'], 'CatTiposAltas'));
        $rules->add($rules->existsIn(['cat_unidade_traslado_id'], 'CatUnidades'));

        return $rules;
    }
    
    public function beforeSave(Event $event, Entity $entity)
    {
     $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
     $modificadas ='aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
     
     $entity->motivo = mb_strtoupper(strtr(utf8_decode($entity->motivo), utf8_decode($originales), $modificadas) ,'utf-8');      
     $entity->observacion = mb_strtoupper(strtr(utf8_decode($entity->observacion), utf8_decode($originales), $modificadas) ,'utf-8');      
     $entity->observacion_alta = mb_strtoupper(strtr(utf8_decode($entity->observacion_alta), utf8_decode($originales), $modificadas) ,'utf-8');      
    }
    
}
