<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OpeExpediente Entity
 *
 * @property string $id
 * @property string $co_usuario_id
 * @property string $cat_unidade_id
 * @property string $cat_area_id
 * @property string $cat_persona_id
 * @property string $cat_atencione_id
 * @property string $cat_tipos_alta_id
 * @property string $cat_unidade_traslado_id
 * @property string $motivo
 * @property \Cake\I18n\FrozenDate $fecha
 * @property \Cake\I18n\FrozenTime $hora
 * @property string $observacion
 * @property string $observacion_alta
 * @property \Cake\I18n\FrozenDate $fecha_alta
 * @property \Cake\I18n\FrozenTime $hora_alta
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\CatPersona $cat_persona
 * @property \App\Model\Entity\CatAtencione $cat_atencione
 * @property \App\Model\Entity\CatTiposAlta $cat_tipos_alta
 * @property \App\Model\Entity\CatUnidade $cat_unidade
 * @property \App\Model\Entity\OpeSeguimientosExpediente[] $ope_seguimientos_expedientes
 */
class OpeExpediente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
