<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OpeSeguimientosExpediente Entity
 *
 * @property string $id
 * @property string $ope_expediente_id
 * @property string $co_usuario_id
 * @property string $cat_estatu_id
 * @property string $observacion
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\OpeExpediente $ope_expediente
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\CatEstatus $cat_estatus
 */
class OpeSeguimientosExpediente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
