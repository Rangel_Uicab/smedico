<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CoGrupo Entity
 *
 * @property int $id
 * @property string $name
 * @property string $pagina_inicial
 * @property bool $activo
 * @property bool $notificacion
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoMenu[] $co_menus
 * @property \App\Model\Entity\CoPermiso[] $co_permisos
 * @property \App\Model\Entity\CoUsuario[] $co_usuarios
 */
class CoGrupo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
