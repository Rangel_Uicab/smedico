<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatUnidade Entity
 *
 * @property string $id
 * @property int $cat_tipos_unidade_id
 * @property int $cat_localidade_id
 * @property int $cat_municipio_id
 * @property string $name
 * @property string $nombre_corto
 * @property string $latitud
 * @property string $longitud
 * @property bool $activo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CatTiposUnidade $cat_tipos_unidade
 * @property \App\Model\Entity\CatLocalidade $cat_localidade
 * @property \App\Model\Entity\CatMunicipio $cat_municipio
 * @property \App\Model\Entity\CatArea[] $cat_areas
 * @property \App\Model\Entity\CoUsuario[] $co_usuarios
 */
class CatUnidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
