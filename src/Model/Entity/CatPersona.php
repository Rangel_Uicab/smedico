<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CatPersona Entity
 *
 * @property string $id
 * @property string $co_usuario_id
 * @property int $cat_municipio_id
 * @property int $cat_localidade_id
 * @property string $nombre
 * @property string $paterno
 * @property string $materno
 * @property string $sexo
 * @property string $num_expediente
 * @property string $num_afiliacion
 * @property \Cake\I18n\FrozenDate $fecha_nacimiento
 * @property string $tel_movil
 * @property string $tel_casa
 * @property string $direccion
 * @property bool $activo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\CoUsuario $co_usuario
 * @property \App\Model\Entity\CatMunicipio $cat_municipio
 * @property \App\Model\Entity\CatLocalidade $cat_localidade
 * @property \App\Model\Entity\OpeExpediente[] $ope_expedientes
 */
class CatPersona extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
    
    protected $_virtual = ['nombre_completo'];

    protected function _getNombreCompleto()
    {
        $Nombre = $this->_properties['nombre'] . '  ' .$this->_properties['paterno'].' '.$this->_properties['materno'];
        
        return $Nombre;
    }
    
}
