<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php 
    echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
    echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catPersona->id],['class'=>'list-group-item','escape'=>false]) ?>				
	    </div> 	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="card border-light mb-3">
                <div class="card-body">                    
                    <h2>Registrar Ingreso del Paciente</h2>
                    <hr>
                     <h4 class="c-grey-900 mB-5">
                        <?php echo $catPersona->nombre_completo?>
                    </h4>
                    
                    <table class="table table-sm table-condensed table-bordered">
                        <tr>
                            <td colspan="2">
                                <span>
                                    <b>Sexo: </b><?= $catPersona->sexo =='H' ? 'Hombre' : 'Mujer' ?>
                                </span>
                                <br>
                                <span>
                                    <b>Fecha Nacimiento: </b><?= date('d-m-Y',strtotime($catPersona->fecha_nacimiento)) ?>
                                </span>
                                <br>
                                <span>
                                    <b>Unidad: </b><?= $catPersona->has('cat_unidade') ? $catPersona->cat_unidade->name : '' ?>
                                </span>
                                <br> 
                                <span>
                                    <b>Tel&eacute;fono Movil: </b><?= h($catPersona->tel_movil) ?> <b>Tel&eacute;fono Casa: </b> <?= h($catPersona->tel_casa) ?>
                                </span>
                                <br>
                            </td>
                        </tr>
                        <tr class="table-active">
                            <th>Expediente</th>
                            <th>Afiliaci&oacute;n Seguro Popular</th>
                        </tr>
                        <tr>
                            <td><?= ($catPersona->num_expediente)? $catPersona->num_expediente : 'S/N'?></td>
                            <td><?= h($catPersona->num_afiliacion) ? $catPersona->num_afiliacion : 'S/A' ?></td>
                        </tr>
                    </table>
				<hr>
                
                <div class="col-md-12">
                        <?= $this->Form->create($opeExpediente,['url'=>['action'=>'add_expediente'],'role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
                                        
                        <?php
                            echo $this->Form->hidden('cat_persona_id', ['value'=>$catPersona->id]);
                            echo $this->Form->control('cat_atencione_id', ['empty'=>true,'escape'=>false,'required' => true,'options' => $catAtenciones,'label'=>['text'=>'Tipo de Atencion']]);
                            echo $this->Form->control('motivo', ['escape'=>false,'required' => true,'label'=>['text'=>'Motivo']]);
                            ?>
                           <?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>
                        <?= $this->Form->end() ?>
                 </div>
                   
				</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#cat-atencione-id").select2({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});   
</script>