<style>
.modal-open .select2-dropdown {
z-index: 10060;
}

.modal-open .select2-close-mask {
z-index: 10055;
}
</style>
<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php
    echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
    echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);

$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
            <?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'busqueda_expedientes'],['class'=>'list-group-item','escape'=>false]) ?>            
        </div>
        
        <div class="bgc-white p-10 bd">
             <h4>RECEPCI&Oacute;N</h4>
             <hr>
             
             <b>UNIDAD: </b><br> 
             <?= $opeExpediente->has('cat_unidade') ? $opeExpediente->cat_unidade->name : '' ?><br>
             <b>AREA: </b><br>
             <?= $opeExpediente->has('cat_area') ? $opeExpediente->cat_area->name : '' ?><br>
             <b>RESPONSABLE RECEPCI&Oacute;N: </b><br>
             <?= $opeExpediente->has('co_usuario') ? $opeExpediente->co_usuario->nombre_completo : '' ?>
             
        </div>
        <div class="bgc-white p-10 bd">
             <h4>ALTA</h4>
             <hr>
             <?php
              if($opeExpediente->has('cat_tipos_alta'))
              {
                  ?>
                  <b>TIPO ALTA: </b><br> 
                 <?= $opeExpediente->has('cat_tipos_alta') ? $opeExpediente->cat_tipos_alta->name : '' ?><br>
                 <?php
                     if($opeExpediente->has('cat_unidades_alta'))
                     {
                       ?>
                       <b>UNIDAD DESTINO: </b><br>
                        <?=  $opeExpediente->cat_unidades_alta->name ?>
                        <br>
                       <?php
                     }
                 ?>
                 <b>OBSERVACI&Oacute;N: </b><br>
                 <?= $opeExpediente->observacion_alta ?>
                  <?php
              }
              else
              {
                ?>
                <div class="alert alert-danger" role="alert">
                  Aun no se ha dado de alta al paciente.
                </div>
                <?php 
              }   
             ?>
             
             
        </div>
        	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                </div>
                <div class="panel-body">
             	    PACIENTE: <h3><?= h($opeExpediente->cat_persona->nombre_completo) ?></h3>
                    <table class="table table-sm table-condensed table-bordered">
                        <tr>
                            <td colspan="2">
                                <span>
                                    <b>SEXO: </b><?= $opeExpediente->cat_persona->sexo =='H' ? 'HOMBRE' : 'MUJER' ?>
                                </span>
                                <br>
                                <span>
                                    <b>FECHA DE NACIMIENTO: </b><?= date('d-m-Y',strtotime($opeExpediente->cat_persona->fecha_nacimiento)) ?>
                                </span>
                                <br> 
                                <span>
                                    <b>TEL. MOVIL: </b>
                                    <?= 
                                        (!empty($opeExpediente->cat_persona->tel_movil)) ?  $opeExpediente->cat_persona->tel_movil : 'S/N'
                                    ?> 
                                    <b>TEL. CASA: </b> 
                                    <?= h($opeExpediente->cat_persona->tel_casa) ?  $opeExpediente->cat_persona->tel_casa : 'S/N' 
                                    ?>
                                </span>
                                <br>
                            </td>
                        </tr>
                        <tr class="table-active">
                            <th>EXPEDIENTE</th>
                            <th>AFILIACI&Oacute;N SEGURO POPULAR</th>
                        </tr>
                        <tr>
                            <td><?= (!empty($opeExpediente->cat_persona->num_expediente)) ? $opeExpediente->cat_persona->num_expediente : 'S/N' ?></td>
                            <td><?= (!empty($opeExpediente->cat_persona->num_afiliacion)) ? $opeExpediente->cat_persona->num_afiliacion : 'S/N' ?></td>
                        </tr>
                    </table>
                    <h4>SEGUIMIENTO MEDICO</h4>
                    
                    <table class="table table-sm table-condensed table-bordered">
                        <tr>
                            <td>
                                <b>TIPO DE ATENCI&Oacute;N: </b>
                                <?= $opeExpediente->has('cat_atencione') ? $opeExpediente->cat_atencione->name : '' ?><br>
                                
                                <b>MOTIVO: </b>
                                <?= $opeExpediente->motivo; ?><br> 
                                
                                <b>FECHA Y HORA DE INGRESO: </b>
                                <?php
                                    echo date('Y-m-d',strtotime($opeExpediente->fecha)).' - '.date('H:i:s a',strtotime($opeExpediente->hora));
                                ?>
                            </td>
                        </tr>
                    </table>
                    
                    <table id="opeExpediente" class="table table-sm table-striped table-bordered">
                        <tr class="table-primary">
                            <th><?= __('AREA') ?></th>
                            <th><?= __('RESPONSABLE') ?></th>
                            <th><?= __('ESTADO DE SALUD') ?></th>
                            <th><?= __('F. CREACI&Oacute;N') ?></th>
                        </tr>
                        <?php if (!empty($opeExpediente->ope_seguimientos_expedientes)): ?>

                        <?php foreach ($opeExpediente->ope_seguimientos_expedientes as $opeSeguimientosExpedientes): ?>
                        <tr>
                            <td><?= ($opeSeguimientosExpedientes->has('cat_area')) ? $opeSeguimientosExpedientes->cat_area->name : '' ?></td>
                            <td><?= ($opeSeguimientosExpedientes->has('co_usuario')) ? $opeSeguimientosExpedientes->co_usuario->nombre_completo : '' ?></td>
                            <td><?= ($opeSeguimientosExpedientes->has('cat_estatus')) ? '<span class="'.$opeSeguimientosExpedientes->cat_estatus->clase.'">'.$opeSeguimientosExpedientes->cat_estatus->name.'</span>' : '' ?></td>
                            <td><?= date('d-m-Y H:i a',strtotime($opeSeguimientosExpedientes->created)) ?></td>
                        </tr>
                        <tr>
                            <td colspan="5">
                             <b>OBSERVACI&Oacute;N: </b><br>
                             <?= h($opeSeguimientosExpedientes->observacion) ?>
                            </td>
                        </tr>
                        
                        <?php endforeach; ?>
                        <?php 
                            else:
                         ?>
                                <tr>
                                    <td colspan="5">SIN SEGUIMIENTOS</td>
                                </tr>
                         <?php   
                            endif;
                         ?>
                    </table>
				</div>
            </div>
        </div>
	</div>
</div>

