<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
  echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
  echo $this->Html->script('select2/select2.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<script type="text/javascript">
    function VerificarNumeros(e)
    {
    var key = window.Event ? e.which : e.keyCode;
    return ((key >= 48 && key <= 57) || (key==8) || (key ==0))
    }
</script>

<div class="row gap-20">
<!--	<div class="col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<? // $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
		</div>
	</div> -->

	<div class="col-md-12">
        <div class="bgc-white p-10 bd">
            <div class="card border-light mb-3">
	           <!-- <div class="card-header">

	            </div>-->
	            <div class="card-body">
                    <h4 class="c-grey-900 mB-20"> <?= __('Recepci&oacute;n de Paciente') ?>
                    <b class="card-title">(<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>) Campos obligatorios</b>
                    </h4>

                    <hr>
        		    <?= $this->Form->create($catPersona,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>

                    <div class="row">
                        <div class="col-md-7">
                        <div class="col-md-12">
                            <h3>Datos del Paciente</h3>
                        </div>
                            <div class="row">
                                <div class="col-md-4"><?php echo $this->Form->control('nombre', ['escape'=>false,'required' => true,'label'=>['text'=>'Nombre(s) <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                                <div class="col-md-4"><?php echo $this->Form->control('paterno', ['escape'=>false,'required' => true,'label'=>['text'=>'A. Paterno <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                                <div class="col-md-4"><?php echo $this->Form->control('materno', ['escape'=>false,'required' => true,'label'=>['text'=>'A. Materno <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                    $sexo = array('H'=>'Hombre','M'=>'Mujer');
                                    echo $this->Form->control('sexo', ['options'=>$sexo,'empty'=>true,'escape'=>false,'required' => true,'label'=>['text'=>'Sexo <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-inline">
                                        <div class="fg-line">
                                            <label>Fecha de Nacimiento</label>
                                              <?php
                                              $Meses = array(
                                                             '01'=>'Enero',
                                                             '02'=>'Febrero',
                                                             '03'=>'Marzo',
                                                             '04'=>'Abril',
                                                             '05'=>'Mayo',
                                                             '06'=>'Junio',
                                                             '07'=>'Julio',
                                                             '08'=>'Agosto',
                                                             '09'=>'Septiembre',
                                                             '10'=>'Octubre',
                                                             '11'=>'Noviembre',
                                                             '12'=>'Diciembre',
                                                            );

                                              echo $this->Form->day('fecha_nacimiento', ['required'=>false,'label'=>['text'=>'Dia']]);
                                              echo $this->Form->month('fecha_nacimiento', ['options'=>$Meses,'required'=>false,'label'=>['text'=>'Mes']]);
                                              echo $this->Form->year('fecha_nacimiento', ['required'=>false,'maxYear' => date('Y'),'minYear' => date('Y') - 100,'label'=>['text'=>'Año']]);
                                              ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                        echo $this->Form->control('entidad_federativa_nacimiento', ['label'=>['text'=>'Entidad de Nacimiento']]);
                                    ?>
                                </div>
                                <div class="col-md-8">
                                    <?php
                                        echo $this->Form->control('curp', ['label'=>['text'=>'CURP']]);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                        echo $this->Form->control('num_expediente', ['label'=>['text'=>'Numero de Expediente']]);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                        echo $this->Form->control('num_afiliacion', ['escape'=>false,'empty' => true,'label'=>['text'=>'Numero Afiliaci&oacute;n (Seguro Popular)']]);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                        echo $this->Form->control('tel_movil', ['type'=>'text','onkeypress'=>"return VerificarNumeros(event)",'maxlength'=>10,'label'=>['text'=>'Tel. Movil']]);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                        echo $this->Form->control('tel_casa', ['type'=>'text','onkeypress'=>"return VerificarNumeros(event)",'maxlength'=>10,'empty' => true,'label'=>['text'=>'Tel. Casa']]);
                                    ?>
                                </div>
                            </div>
                            <h4>DOMICILIO</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <?php
                                        echo $this->Form->control('localidad', ['label'=>['text'=>'Localidad']]);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                        echo $this->Form->control('municipio', ['label'=>['text'=>'Municipio']]);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                        echo $this->Form->control('entidad_federativa', ['label'=>['text'=>'Entidad']]);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <?php
                                        echo $this->Form->control('tipo_asentamiento', ['label'=>['text'=>'Tipo Asentamiento']]);
                                    ?>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                        echo $this->Form->control('nombre_asentamiento', ['label'=>['text'=>'Nombre de Asentamiento']]);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                        echo $this->Form->control('vialidad', ['label'=>['text'=>'Nombre de Vialidad']]);
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                        echo $this->Form->control('nexterior', ['label'=>['text'=>'N.Ext.']]);
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                        echo $this->Form->control('ninterior', ['label'=>['text'=>'N.Int.']]);
                                    ?>
                                </div>
                                <div class="col-md-2">
                                    <?php
                                        echo $this->Form->control('codigo_postal', ['label'=>['text'=>'C.Postal']]);
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Motivo de Atenci&oacute;n</h3>
                                </div>
                                <div class="col-md-12">
                                <?php
                                    echo $this->Form->control('ope_expedientes.0.cat_atencione_id', ['options'=>$catAtenciones,'empty'=>true,'escape'=>false,'required' => true,'label'=>['text'=>'Tipo de Atenci&oacute;n <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                ?>
                                </div>
                                <div class="col-md-12">
                                <?php
                                    echo $this->Form->control('ope_expedientes.0.motivo', ['type'=>'textarea','placeholder'=>'Redaccion del motivo de atenci&oacute;n','escape'=>false,'required' => true,'label'=>['text'=>'Motivo de Atenci&oacute;n <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                ?>
                                </div>
                                <div class="col-md-12">
                                <?php
                                    echo $this->Form->control('ope_expedientes.0.privado', ['escape'=>false,'label'=>['text'=>' <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Marcar como privado el seguimiento del paciente</span>']]);
                                ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $this->Form->button('<i class="ti-user"></i>&nbsp; Guardar Paciente',array('type'=>'submit','class'=>'btn btn-block btn-primary waves-effect','id'=>'btnGuardar')) ?>
                                </div>
                            </div>
                        </div>



                    </div>
        		    <?= $this->Form->end() ?>
    		    </div>
            </div>
	    </div>
	</div>
</div>

<script type="text/javascript">
    $("#ope-expedientes-0-cat-atencione-id").select2({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});
    // $('#ope-expedientes-0-cat-atencione-id').selectpicker({
    //                                                         style: 'btn-default',
    //                                                         size: 4
    //                                                       });

</script>
