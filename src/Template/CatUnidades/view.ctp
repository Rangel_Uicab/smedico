<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catUnidade->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catUnidade->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catUnidade->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catUnidade->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= h($catUnidade->id) ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Tipos Unidade') ?></th>
				                <td><?= $catUnidade->has('cat_tipos_unidade') ? $this->Html->link($catUnidade->cat_tipos_unidade->name, ['controller' => 'CatTiposUnidades', 'action' => 'view', $catUnidade->cat_tipos_unidade->id]) : '' ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Localidade') ?></th>
				                <td><?= $catUnidade->has('cat_localidade') ? $this->Html->link($catUnidade->cat_localidade->name, ['controller' => 'CatLocalidades', 'action' => 'view', $catUnidade->cat_localidade->id]) : '' ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Municipio') ?></th>
				                <td><?= $catUnidade->has('cat_municipio') ? $this->Html->link($catUnidade->cat_municipio->name, ['controller' => 'CatMunicipios', 'action' => 'view', $catUnidade->cat_municipio->id]) : '' ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($catUnidade->name) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Nombre Corto') ?></th>
				                <td><?= h($catUnidade->nombre_corto) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Latitud') ?></th>
				                <td><?= h($catUnidade->latitud) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Longitud') ?></th>
				                <td><?= h($catUnidade->longitud) ?></td>
				            </tr>
				    				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catUnidade->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($catUnidade->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $catUnidade->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="related">
				            <h4><?= __('Related Cat Areas') ?></h4>
				            <?php if (!empty($catUnidade->cat_areas)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Id') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Descripcion') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catUnidade->cat_areas as $catAreas): ?>
				                <tr>
				                    <td><?= h($catAreas->id) ?></td>
				                    <td><?= h($catAreas->cat_unidade_id) ?></td>
				                    <td><?= h($catAreas->name) ?></td>
				                    <td><?= h($catAreas->descripcion) ?></td>
				                    <td><?= h($catAreas->activo) ?></td>
				                    <td><?= h($catAreas->created) ?></td>
				                    <td><?= h($catAreas->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CatAreas', 'action' => 'view', $catAreas->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CatAreas', 'action' => 'edit', $catAreas->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CatAreas', 'action' => 'delete', $catAreas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $catAreas->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				    				        <div class="related">
				            <h4><?= __('Related Co Usuarios') ?></h4>
				            <?php if (!empty($catUnidade->co_usuarios)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Area Id') ?></th>
				    				                    <th scope="col"><?= __('Nombre') ?></th>
				    				                    <th scope="col"><?= __('Paterno') ?></th>
				    				                    <th scope="col"><?= __('Materno') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Login') ?></th>
				    				                    <th scope="col"><?= __('Password') ?></th>
				    				                    <th scope="col"><?= __('Ultimo Acceso') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catUnidade->co_usuarios as $coUsuarios): ?>
				                <tr>
				                    <td><?= h($coUsuarios->id) ?></td>
				                    <td><?= h($coUsuarios->cat_unidade_id) ?></td>
				                    <td><?= h($coUsuarios->cat_area_id) ?></td>
				                    <td><?= h($coUsuarios->nombre) ?></td>
				                    <td><?= h($coUsuarios->paterno) ?></td>
				                    <td><?= h($coUsuarios->materno) ?></td>
				                    <td><?= h($coUsuarios->activo) ?></td>
				                    <td><?= h($coUsuarios->login) ?></td>
				                    <td><?= h($coUsuarios->password) ?></td>
				                    <td><?= h($coUsuarios->ultimo_acceso) ?></td>
				                    <td><?= h($coUsuarios->created) ?></td>
				                    <td><?= h($coUsuarios->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoUsuarios', 'action' => 'view', $coUsuarios->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoUsuarios', 'action' => 'edit', $coUsuarios->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoUsuarios', 'action' => 'delete', $coUsuarios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coUsuarios->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
