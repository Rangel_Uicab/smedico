<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php
  echo $this->Html->css(array('datatables/datatables.net-responsive-bs4/responsive.bootstrap4.min' ));
  echo $this->Html->script(array('datatables/jquery.dataTables.min','datatables/datatables.net-responsive/dataTables.responsive.minfd53.js?v4.0.1'));  
?>

<div class="row gap-20">
	<div class="col-md-12">
        <div class="bgc-white p-10 bd"> 
	       <div class="col-md-12">
               <div class="text-center">
	             <h2>Administraci&oacute;n de Cat Personas</h2>        
	           </div> 
	            <div class="text-right">
	                <?php echo $this->Html->link('<i class="ti-plus" aria-hidden="true"></i>&nbsp; Nuevo Registro' , ['action' => 'add'],['escape'=>false,'class'=>'btn btn-outline-primary']) ?>
	            </div>
	            <br>
	        </div>
	    
	        <div class="col-md-12">
	 	        <table id="catPersona" class="table table-striped table-bordered dataTable">
	                <thead>
	                     <tr>
	        	                        <th>id</th>
	        	                        <th>co_usuario_id</th>
	        	                        <th>cat_municipio_id</th>
	        	                        <th>cat_localidade_id</th>
	        	                        <th>nombre</th>
	        	                        <th>paterno</th>
	        	                        <th>materno</th>
	        	                        <th>sexo</th>
	        	                        <th>num_expediente</th>
	        	                        <th>num_afiliacion</th>
	        	                        <th>fecha_nacimiento</th>
	        	                        <th>tel_movil</th>
	        	                        <th>tel_casa</th>
	        	                        <th>activo</th>
	        	                        <th>created</th>
	        	                        <th>modified</th>
	        	                        <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <tr>
	                        <td colspan="17">Cargando datos...</td>
	                    </tr>
	                </tbody>
	            </table>
            </div> 
        </div> 
	</div> 
 </div> 
 
<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults, 
    { 
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#catPersona');

    var tabla = $('#catPersona').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            'responsive': true,
            "sAjaxSource": "<?php echo $this->Url->build(array('action'=>'get_data','_ext'=>'json')); ?>", 
//			'dom': 'Bfrtip',	
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Buscar...",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            { "bSortable": false, "aTargets": [16] }
                                    ]
        });
     
</script>