<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catPersona->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catPersona->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catPersona->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catPersona->id) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= h($catPersona->id) ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Co Usuario') ?></th>
				                <td><?= $catPersona->has('co_usuario') ? $this->Html->link($catPersona->co_usuario->nombre_completo, ['controller' => 'CoUsuarios', 'action' => 'view', $catPersona->co_usuario->id]) : '' ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Municipio') ?></th>
				                <td><?= $catPersona->has('cat_municipio') ? $this->Html->link($catPersona->cat_municipio->name, ['controller' => 'CatMunicipios', 'action' => 'view', $catPersona->cat_municipio->id]) : '' ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Localidade') ?></th>
				                <td><?= $catPersona->has('cat_localidade') ? $this->Html->link($catPersona->cat_localidade->name, ['controller' => 'CatLocalidades', 'action' => 'view', $catPersona->cat_localidade->id]) : '' ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Nombre') ?></th>
				                <td><?= h($catPersona->nombre) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Paterno') ?></th>
				                <td><?= h($catPersona->paterno) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Materno') ?></th>
				                <td><?= h($catPersona->materno) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Sexo') ?></th>
				                <td><?= h($catPersona->sexo) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Num Expediente') ?></th>
				                <td><?= h($catPersona->num_expediente) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Num Afiliacion') ?></th>
				                <td><?= h($catPersona->num_afiliacion) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Tel Movil') ?></th>
				                <td><?= h($catPersona->tel_movil) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Tel Casa') ?></th>
				                <td><?= h($catPersona->tel_casa) ?></td>
				            </tr>
				    				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Fecha Nacimiento') ?></th>
				                <td><?= h($catPersona->fecha_nacimiento) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catPersona->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($catPersona->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $catPersona->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="row">
				            <h4><?= __('Direccion') ?></h4>
				            <?= $this->Text->autoParagraph(h($catPersona->direccion)); ?>
				        </div>
				    				    				    				        <div class="related">
				            <h4><?= __('Related Ope Expedientes') ?></h4>
				            <?php if (!empty($catPersona->ope_expedientes)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Co Usuario Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Persona Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Atencione Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Tipos Alta Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Traslado Id') ?></th>
				    				                    <th scope="col"><?= __('Motivo') ?></th>
				    				                    <th scope="col"><?= __('Fecha') ?></th>
				    				                    <th scope="col"><?= __('Hora') ?></th>
				    				                    <th scope="col"><?= __('Observacion') ?></th>
				    				                    <th scope="col"><?= __('Observacion Alta') ?></th>
				    				                    <th scope="col"><?= __('Fecha Alta') ?></th>
				    				                    <th scope="col"><?= __('Hora Alta') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catPersona->ope_expedientes as $opeExpedientes): ?>
				                <tr>
				                    <td><?= h($opeExpedientes->id) ?></td>
				                    <td><?= h($opeExpedientes->co_usuario_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_persona_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_atencione_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_tipos_alta_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_unidade_traslado_id) ?></td>
				                    <td><?= h($opeExpedientes->motivo) ?></td>
				                    <td><?= h($opeExpedientes->fecha) ?></td>
				                    <td><?= h($opeExpedientes->hora) ?></td>
				                    <td><?= h($opeExpedientes->observacion) ?></td>
				                    <td><?= h($opeExpedientes->observacion_alta) ?></td>
				                    <td><?= h($opeExpedientes->fecha_alta) ?></td>
				                    <td><?= h($opeExpedientes->hora_alta) ?></td>
				                    <td><?= h($opeExpedientes->created) ?></td>
				                    <td><?= h($opeExpedientes->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'OpeExpedientes', 'action' => 'view', $opeExpedientes->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'OpeExpedientes', 'action' => 'edit', $opeExpedientes->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'OpeExpedientes', 'action' => 'delete', $opeExpedientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $opeExpedientes->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
