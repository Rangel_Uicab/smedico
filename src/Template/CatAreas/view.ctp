<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catArea->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catArea->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catArea->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catArea->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= h($catArea->id) ?></td>
				            </tr>
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Unidade') ?></th>
				                <td><?= $catArea->has('cat_unidade') ? $this->Html->link($catArea->cat_unidade->name, ['controller' => 'CatUnidades', 'action' => 'view', $catArea->cat_unidade->id]) : '' ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($catArea->name) ?></td>
				            </tr>
				    				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catArea->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($catArea->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $catArea->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="row">
				            <h4><?= __('Descripcion') ?></h4>
				            <?= $this->Text->autoParagraph(h($catArea->descripcion)); ?>
				        </div>
				    				    				    				        <div class="related">
				            <h4><?= __('Related Co Usuarios') ?></h4>
				            <?php if (!empty($catArea->co_usuarios)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Area Id') ?></th>
				    				                    <th scope="col"><?= __('Nombre') ?></th>
				    				                    <th scope="col"><?= __('Paterno') ?></th>
				    				                    <th scope="col"><?= __('Materno') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Login') ?></th>
				    				                    <th scope="col"><?= __('Password') ?></th>
				    				                    <th scope="col"><?= __('Ultimo Acceso') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catArea->co_usuarios as $coUsuarios): ?>
				                <tr>
				                    <td><?= h($coUsuarios->id) ?></td>
				                    <td><?= h($coUsuarios->cat_unidade_id) ?></td>
				                    <td><?= h($coUsuarios->cat_area_id) ?></td>
				                    <td><?= h($coUsuarios->nombre) ?></td>
				                    <td><?= h($coUsuarios->paterno) ?></td>
				                    <td><?= h($coUsuarios->materno) ?></td>
				                    <td><?= h($coUsuarios->activo) ?></td>
				                    <td><?= h($coUsuarios->login) ?></td>
				                    <td><?= h($coUsuarios->password) ?></td>
				                    <td><?= h($coUsuarios->ultimo_acceso) ?></td>
				                    <td><?= h($coUsuarios->created) ?></td>
				                    <td><?= h($coUsuarios->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoUsuarios', 'action' => 'view', $coUsuarios->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoUsuarios', 'action' => 'edit', $coUsuarios->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoUsuarios', 'action' => 'delete', $coUsuarios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coUsuarios->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
