<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php 
//	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
//  echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<div class="row gap-20">
	<div class="col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
        </div>
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
            <div class="card border-light mb-3">
	            
	            <div class="card-body">
                <h4 class="c-grey-900 mB-20"> <?= __('Actualizar Informaci&oacute;n del Paciente') ?>
                <b class="card-title">(<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>) Campos obligatorios</b>
                </h4>
                
                <hr>

        		    <?= $this->Form->create($catPersona,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        	        			    
				    <div class="row">
                        <div class="col-md-4"><?php echo $this->Form->control('nombre', ['escape'=>false,'required' => true,'label'=>['text'=>'Nombre(s) <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                        <div class="col-md-4"><?php echo $this->Form->control('paterno', ['escape'=>false,'required' => true,'label'=>['text'=>'A. Paterno <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                        <div class="col-md-4"><?php echo $this->Form->control('materno', ['escape'=>false,'required' => true,'label'=>['text'=>'A. Materno <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);?></div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            $sexo = array('H'=>'Hombre','M'=>'Mujer');
                            echo $this->Form->control('sexo', ['options'=>$sexo,'empty'=>true,'escape'=>false,'required' => true,'label'=>['text'=>'sexo <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-inline">
                                <div class="fg-line">
                                    <label>Fecha de Nacimiento <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span></label>
                                      <?php                                  
                                      $Meses = array(
                                                     '01'=>'Enero',   
                                                     '02'=>'Febrero',   
                                                     '03'=>'Marzo',   
                                                     '04'=>'Abril',   
                                                     '05'=>'Mayo',   
                                                     '06'=>'Junio',   
                                                     '07'=>'Julio',   
                                                     '08'=>'Agosto',   
                                                     '09'=>'Septiembre',   
                                                     '10'=>'Octubre',   
                                                     '11'=>'Noviembre',   
                                                     '12'=>'Diciembre',   
                                                    );
                                      
                                      echo $this->Form->day('fecha_nacimiento', ['required'=>true,'label'=>['text'=>'Dia']]);
                                      echo $this->Form->month('fecha_nacimiento', ['options'=>$Meses,'required'=>true,'label'=>['text'=>'Mes']]);   
                                      echo $this->Form->year('fecha_nacimiento', ['required'=>true,'maxYear' => date('Y'),'minYear' => date('Y') - 100,'label'=>['text'=>'Año']]);  
                                      ?>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                echo $this->Form->control('tel_movil', ['type'=>'text','onkeypress'=>"return VerificarNumeros(event)",'maxlength'=>10,'label'=>['text'=>'Tel. Movil']]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                echo $this->Form->control('tel_casa', ['type'=>'text','onkeypress'=>"return VerificarNumeros(event)",'maxlength'=>10,'empty' => true,'label'=>['text'=>'Tel. Casa']]);
                            ?>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                echo $this->Form->control('num_expediente', ['label'=>['text'=>'Numero de Expediente']]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                echo $this->Form->control('num_afiliacion', ['escape'=>false,'empty' => true,'label'=>['text'=>'Numero Afiliaci&oacute;n (Seguro Popular)']]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->button('<i class="ti-user"></i>&nbsp; Actualizar Paciente',array('type'=>'submit','class'=>'btn btn-block btn-primary waves-effect','id'=>'btnGuardar')) ?>
                        </div>
                    </div>

        		    <?= $this->Form->end() ?>
    		    </div>
            </div>
	    </div>
        
	</div>
</div>


<script type="text/javascript">
//    $("#cat-localidade-id").select2({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});
</script>