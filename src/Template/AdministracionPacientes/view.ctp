<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php 
    echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
    echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<div class="row gap-20">
	<div class="masonry-item col-md-2">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>                                     
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catPersona->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catPersona->id],['confirm' => __('Realmente desea eliminar al Paciente :{0}?', $catPersona->nombre_completo),'escape'=>false,'class'=>'list-group-item'])?>
        </div> 	
	</div>

	<div class="col-md-10">
        <div class="bgc-white p-10 bd">
		    <div class="card border-light mb-3">
                <div class="card-body">                    
                    <h2>Historial del Paciente</h2>
                    <hr>
                     <h4 class="c-grey-900 mB-5">
                        <?php echo $catPersona->nombre_completo?>
                    </h4>
                    
                    <table class="table table-sm table-condensed table-bordered">
                        <tr>
                            <td colspan="2">
                                <span>
                                    <b>Sexo: </b><?= $catPersona->sexo =='H' ? 'Hombre' : 'Mujer' ?>
                                </span>
                                <br>
                                <span>
                                    <b>Fecha Nacimiento: </b><?= date('d-m-Y',strtotime($catPersona->fecha_nacimiento)) ?>
                                </span>
                                <br>
                                <span>
                                    <b>Unidad: </b><?= $catPersona->has('cat_unidade') ? $catPersona->cat_unidade->name : '' ?>
                                </span>
                                <br> 
                                <span>
                                    <b>Tel&eacute;fono Movil: </b><?= h($catPersona->tel_movil) ?> <b>Tel&eacute;fono Casa: </b> <?= h($catPersona->tel_casa) ?>
                                </span>
                                <br>
                            </td>
                        </tr>
                        <tr class="table-active">
                            <th>Expediente</th>
                            <th>Afiliaci&oacute;n Seguro Popular</th>
                        </tr>
                        <tr>
                            <td><?= ($catPersona->num_expediente)? $catPersona->num_expediente : 'S/N'?></td>
                            <td><?= h($catPersona->num_afiliacion) ? $catPersona->num_afiliacion : 'S/A' ?></td>
                        </tr>
                    </table>
				    <hr>
                   
                   <?php
                      if($catPersona->has('ope_expedientes'))
                        {
                            if(!empty($catPersona->ope_expedientes))
                            {
                              ?>
                                <table class="table table-sm table-condensed table-bordered">
                                    <thead>
                                    <tr class="table-success">
                                            <th>Folio</th>
                                            <th>Ingreso</th>
                                            <th>Atenci&oacute;n</th>
                                            <th>Motivo</th>
                                            <th>Fecha Ingreso</th>
                                            <th>Alta</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                              <?php
                              foreach($catPersona->ope_expedientes as $Expediente)
                              {
                              ?>
                                    <tr>
                                        <td><?= $Expediente->folio?></td> 
                                        <td><?= $Expediente->has('cat_area') ? $Expediente->cat_area->name : '' ?></td> 
                                        <td><?= $Expediente->has('cat_atencione') ? $Expediente->cat_atencione->name : '' ?></td> 
                                        <td><?= $Expediente->motivo ?></td> 
                                        <td>
                                            <?php 
                                                echo date('d-m-Y',strtotime($Expediente->fecha)).' '.date('H:i a',strtotime($Expediente->hora)) 
                                            ?>
                                        </td> 
                                        <td>
                                            <?php 
                                                if($Expediente->has('cat_tipos_alta'))
                                                {
                                                    echo '<b>Tipo: </b>'. 
                                                    $Expediente->cat_tipos_alta->name.
                                                    ' <br> <b>Fecha: </b>'.date('d-m-Y',strtotime($Expediente->fecha_alta)).
                                                    ' '.date('H:i a',strtotime($Expediente->hora_alta)).
                                                    '<br> <b>Observacion: </b>'.$Expediente->observacion_alta;   
                                                }
                                                else
                                                {
                                                    echo 'No se ha dado de alta al Paciente.'; 
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal<?php echo $Expediente->id?>">
                                              <i class="ti-eye"> </i>
                                            </button>
                                            
                                            <!-- Modal -->
                                            <div class="modal fade" id="Modal<?php echo $Expediente->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Seguimiento del Paciente</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <b>Tipo de Atenci&oacute;n: </b> <?= $Expediente->has('cat_atencione') ? $Expediente->cat_atencione->name : '' ?> <br>
                                                    <b>Motivo: </b><?= $Expediente->motivo ?>
                                                    <hr>
                                                    
                                                    <table id="opeExpediente" class="table table-sm table-hover table-bordered">
                                                    <tr class="table-primary">
                                                        <th><?= __('AREA') ?></th>
                                                        <th><?= __('RESPONSABLE') ?></th>
                                                        <th><?= __('ESTADO DE SALUD') ?></th>
                                                        <th><?= __('F. CREACI&Oacute;N') ?></th>
                                                    </tr>
                                                    <?php if (!empty($Expediente->ope_seguimientos_expedientes)): ?>

                                                    <?php foreach ($Expediente->ope_seguimientos_expedientes as $opeSeguimientosExpedientes): ?>
                                                    <tr>
                                                        <td><?= ($opeSeguimientosExpedientes->has('cat_area')) ? $opeSeguimientosExpedientes->cat_area->name : '' ?></td>
                                                        <td><?= ($opeSeguimientosExpedientes->has('co_usuario')) ? $opeSeguimientosExpedientes->co_usuario->nombre_completo : '' ?></td>
                                                        <td><?= ($opeSeguimientosExpedientes->has('cat_estatus')) ? '<span class="'.$opeSeguimientosExpedientes->cat_estatus->clase.'">'.$opeSeguimientosExpedientes->cat_estatus->name.'</span>' : '' ?></td>
                                                        <td><?= date('d-m-Y H:i a',strtotime($opeSeguimientosExpedientes->created)) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                             <b>OBSERVACI&Oacute;N: </b><br>
                                                             <?= h($opeSeguimientosExpedientes->observacion) ?>
                                                            </td>
                                                        </tr>
                                                        
                                                        <?php endforeach; ?>
                                                        <?php 
                                                            else:
                                                         ?>
                                                                <tr>
                                                                    <td colspan="4">SIN SEGUIMIENTOS</td>
                                                                </tr>
                                                         <?php   
                                                            endif;
                                                         ?>
                                                    </table>
                                                    
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            
                                        </td> 
                                    </tr>
                                    
                              <?php
                              }
                              ?>
                                    </tbody>
                                </table>
                              <?php  
                            }
                            else
                            {
                                
                            }
                        }
                    ?>
                   
                   
				</div>
            </div>
        </div> 
    </div>
</div>
<script type="text/javascript">
//    $("#cat-atencione-id").select2({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});   
</script>