<style>
.modal-open .select2-dropdown {
z-index: 10060;
}

.modal-open .select2-close-mask {
z-index: 10055;
}
</style>
<?php 

    $this->loadHelper('Form', ['templates' => 'app_form']);

    if($catTiposAltas->requiere_unidad == 1)
    {
        echo $this->Form->control('cat_unidade_traslado_id', ['options' => $catUnidades, 'empty' => true,'label'=>['text'=>'Unidad destino']]);
    }
?>

<script type="text/javascript">
       $("#cat-unidade-traslado-id").select2({
            dropdownParent: $("#ModalAltaPaciente"),
                                  
//           dropdownParent: "#ModalAltaPaciente" ,
           placeholder: "SELECCIONAR",allowClear: true, width:'100%'});    
</script>