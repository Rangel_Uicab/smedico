<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<div class="row">
	<div class="col-md-12">
          <div class="col-md-12" style="text-align: center;">
          <h1 class="text-info">HOSPITAL MATERNO INFANTIL MORELOS</h1>
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo2.jpg"
              title="Remark">
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo.png"
              title="Remark">
          </div>
          <div class="col-md-12" style="text-align: center;">
            <h2>BIENVENIDO(A)</h2>
            <h3>INGRESE SU N&Uacute;MERO DE FOLIO</h3>

            <div class="row justify-content-center">
                <div class="col-3">
                <?php
                    $onClick = $this->Ajax->RemoteFunction([
                                                            'type'=>'POST',
                                                            'data'=>"{id:folio.value,unidad_id:'91067801-5a19-42e3-b9d0-80f57aef0dc0'}",
                                                            'url'=>['action'=>'getExpediente'],
                                                            'update'=>'DivExpediente',
                                                            'before' => "$('#Loading').fadeIn();",
                                                            'complete' => "$('#Loading').fadeOut();"
                                                            ]);
                ?>

                  <?php echo $this->Form->control('folio', ['autocomplete'=>'off','maxlength'=>"5",'placeholder'=>'Ingresar Numero de Folio','escape'=>false,'required' => true,'label'=>['text'=>'']]);?>
                </div>
                <div class="col-2"><br>
                <?php echo $this->Form->button('<i class="ti-search" aria-hidden="true"></i> Buscar',array('onClick'=>$onClick,'type'=>'submit','class'=>'btn btn-block btn-primary','id'=>'btnBuscarFolio')) ?>
                </div>
            </div>

          </div>

          <div class="col-md-12" id="DivExpediente"></div>

          <div class="col-md-12" id="Loading" style="display: none; text-align: center;">
            <img src="<?php echo $this->request->webroot; ?>img/procesando.gif">
          </div>
	</div>
 </div>


 <script type="text/javascript">
 $(document).ready(function()
    {
        $('#btnBuscarFolio').attr('disabled',true);
        $('#folio').keyup(function()
        {
            if($(this).val().length !=0)
                $('#btnBuscarFolio').attr('disabled', false);
            else
                $('#btnBuscarFolio').attr('disabled',true);
        })
    });
 </script>
