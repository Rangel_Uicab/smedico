<div class="col-md-12">
    <?php
    if(!empty($opeExpediente))
    {
    ?>
    <div class="masonry-item col-md-12">
        <div class="bd bgc-white">
            <div class="layers">
                <div class="layer w-100 p-20">
                    <h6 class="lh-1">SEGUIMIENTO M&Eacute;DICO</h6>
                </div>
                <div class="layer w-100">
                    <div class="bgc-light-blue-500 c-white p-20">
                        <div class="peers ai-c jc-sb gap-40">
                            <div class="peer peer-greed">
                                PACIENTE: <h4><?= h($opeExpediente->cat_persona->nombre_completo) ?></h4>
                                <p class="mB-0">
                                    <b>SEXO: </b><?= $opeExpediente->cat_persona->sexo =='H' ? 'HOMBRE' : 'MUJER' ?>
                                <br>
                                    <b>TEL. MOVIL: </b>
                                    <?= (!empty($opeExpediente->cat_persona->tel_movil)) ?  $opeExpediente->cat_persona->tel_movil : 'S/N' ?>
                                    <b>TEL. CASA: </b>
                                    <?= h($opeExpediente->cat_persona->tel_casa) ?  $opeExpediente->cat_persona->tel_casa : 'S/N' ?>
                                <br>
                                    <b>AFILIACI&Oacute;N SEG. POPULAR: </b>
                                    <?= h($opeExpediente->num_afiliacion) ?  $opeExpediente->num_afiliacion : 'S/N' ?>
                                </p>
                            </div>
                            <div class="peer">
                                <h3 class="text-right">
                                <?php
                                    echo $this->Html->link('<i class="ti-close"></i> TERMINAR CONSULTA','javascript:;',['id'=>'BtnTerminarConsulta','class'=>'btn cur-p btn-dark','escape'=>false]);
                                ?>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive p-20">
                        <table class="table">
                        <tr>
                            <td>
                                <b>HOSPITAL: </b>
                                <?= $opeExpediente->has('cat_unidade') ? $opeExpediente->cat_unidade->name : '' ?><br>

                                <b>AREA DE RECEPCI&Oacute;N: </b>
                                <?= $opeExpediente->has('cat_area') ? $opeExpediente->cat_area->name : '' ?><br>

                                <b>TIPO DE ATENCI&Oacute;N: </b>
                                <?= $opeExpediente->has('cat_atencione') ? $opeExpediente->cat_atencione->name : '' ?><br>

                                <b>MOTIVO: </b>
                                <?= $opeExpediente->motivo; ?><br>

                                <b>FECHA Y HORA DE INGRESO: </b>
                                <?php
                                    echo date('Y-m-d',strtotime($opeExpediente->fecha)).' - '.date('H:i:s a',strtotime($opeExpediente->hora));
                                ?>
                            </td>
                        </tr>
                    </table>
                    <h3>SEGUIMIENTO M&Eacute;DICO</h3>
                    <table id="opeExpediente" class="table table-sm">
                        <tr>
                            <th class="bdwT-0"><?= __('AREA') ?></th>
                            <th class="bdwT-0"><?= __('ESTADO DE SALUD') ?></th>
                            <th class="bdwT-0"><?= __('OBSERVACI&Oacute;N') ?></th>
                            <th class="bdwT-0"><?= __('F. CREACI&Oacute;N') ?></th>
                        </tr>
                        <?php if (!empty($opeExpediente->ope_seguimientos_expedientes)): ?>

                        <?php foreach ($opeExpediente->ope_seguimientos_expedientes as $opeSeguimientosExpedientes): ?>
                        <tr>
                            <td class="fw-600"><?= ($opeSeguimientosExpedientes->has('cat_area')) ? $opeSeguimientosExpedientes->cat_area->name : '' ?></td>
                            <td class="fw-600"><?= ($opeSeguimientosExpedientes->has('cat_estatus')) ? '<span class="'.$opeSeguimientosExpedientes->cat_estatus->clase.'">'.$opeSeguimientosExpedientes->cat_estatus->name.'</span>' : '' ?></td>
                            <td class="fw-600"> <?= h($opeSeguimientosExpedientes->observacion) ?></td>
                            <td><?= date('d-m-Y H:i a',strtotime($opeSeguimientosExpedientes->created)) ?></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php
                            else:
                         ?>
                            <tr>
                                <td colspan="4">
                                    <div class="alert alert-info" role="alert">
                                      <strong>SIN REGISTRO DE SEGUIMIENTO M&Eacute;DICO POR EL MOMENTO.</strong>
                                    </div>
                                </td>
                            </tr>
                         <?php
                            endif;
                         ?>
                    </table>
                    </div>
                </div>
            </div>
        <div class="ta-c bdT w-100 p-20">
            <a href="#"><i class="ti-info-alt"></i> VER HORARIOS DE VISITA</a>
        </div>
    </div>
</div>
    <?php
    }
    else
    {
        ?>
        <div class="alert alert-danger" role="alert">
          <strong>Folio no encontrado!</strong> Verifica que el folio ingresado sea el correcto.
        </div>
        <?php
    }
    ?>
</div>

<script type="text/javascript">

  $('#BtnTerminarConsulta').click(function()
    {
        $('#folio').val("");
        $('#btnBuscarFolio').attr('disabled',true);
        $("#DivExpediente").html("");
        $.notify({
                    title: "<strong>Gracias por su visita:</strong> ",
                    message: "",
                    animate: {
                                    enter: 'animated zoomInDown',
                                    exit: 'animated zoomOutUp'
                                }
                });

    });
</script>
