<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
%>
<?php
/**
  * @var \<%= $namespace %>\View\AppView $this
  */
?>
<%
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}

%>
<?php
  echo $this->Html->css(array('datatables/datatables.net-responsive-bs4/responsive.bootstrap4.min' ));
  echo $this->Html->script(array('datatables/jquery.dataTables.min','datatables/datatables.net-responsive/dataTables.responsive.minfd53.js?v4.0.1'));  
?>

<div class="row gap-20">
	<div class="col-md-12">
        <div class="bgc-white p-10 bd"> 
	       <div class="col-md-12">
               <div class="text-center">
	             <h2>Administraci&oacute;n de <%= $pluralHumanName %></h2>        
	           </div> 
	            <div class="text-right">
	                <?php echo $this->Html->link('<i class="ti-plus" aria-hidden="true"></i>&nbsp; Nuevo Registro' , ['action' => 'add'],['escape'=>false,'class'=>'btn btn-outline-primary']) ?>
	            </div>
	            <br>
	        </div>
	    
	        <div class="col-md-12">
	 	        <table id="<%= $singularVar %>" class="table table-striped table-bordered dataTable">
	                <thead>
	                     <tr>
	        <% $i = 1; foreach ($fields as $field): %>
	                        <th><%= $field %></th>
	        <% $i++; endforeach; %>
	                        <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <tr>
	                        <td colspan="<%= $i %>">Cargando datos...</td>
	                    </tr>
	                </tbody>
	            </table>
            </div> 
        </div> 
	</div> 
 </div> 
 
<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults, 
    { 
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#<%= $singularVar %>');

    var tabla = $('#<%= $singularVar %>').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            'responsive': true,
            "sAjaxSource": "<?php echo $this->Url->build(array('action'=>'get_data','_ext'=>'json')); ?>", 
//			'dom': 'Bfrtip',	
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Buscar...",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            { "bSortable": false, "aTargets": [<%= ($i-1) %>] }
                                    ]
        });
     
</script>