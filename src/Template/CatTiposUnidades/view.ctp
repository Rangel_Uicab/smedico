<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catTiposUnidade->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catTiposUnidade->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catTiposUnidade->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catTiposUnidade->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($catTiposUnidade->name) ?></td>
				            </tr>
				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= $this->Number->format($catTiposUnidade->id) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catTiposUnidade->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($catTiposUnidade->modified) ?></td>
				            </tr>
				    				    				    				        </table>
				    				    				        <div class="related">
				            <h4><?= __('Related Cat Unidades') ?></h4>
				            <?php if (!empty($catTiposUnidade->cat_unidades)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Tipos Unidade Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Localidade Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Municipio Id') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Nombre Corto') ?></th>
				    				                    <th scope="col"><?= __('Latitud') ?></th>
				    				                    <th scope="col"><?= __('Longitud') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catTiposUnidade->cat_unidades as $catUnidades): ?>
				                <tr>
				                    <td><?= h($catUnidades->id) ?></td>
				                    <td><?= h($catUnidades->cat_tipos_unidade_id) ?></td>
				                    <td><?= h($catUnidades->cat_localidade_id) ?></td>
				                    <td><?= h($catUnidades->cat_municipio_id) ?></td>
				                    <td><?= h($catUnidades->name) ?></td>
				                    <td><?= h($catUnidades->nombre_corto) ?></td>
				                    <td><?= h($catUnidades->latitud) ?></td>
				                    <td><?= h($catUnidades->longitud) ?></td>
				                    <td><?= h($catUnidades->activo) ?></td>
				                    <td><?= h($catUnidades->created) ?></td>
				                    <td><?= h($catUnidades->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CatUnidades', 'action' => 'view', $catUnidades->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CatUnidades', 'action' => 'edit', $catUnidades->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CatUnidades', 'action' => 'delete', $catUnidades->id], ['confirm' => __('Are you sure you want to delete # {0}?', $catUnidades->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
