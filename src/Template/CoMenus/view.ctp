<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $coMenu->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $coMenu->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $coMenu->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($coMenu->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Icon') ?></th>
				                <td><?= h($coMenu->icon) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($coMenu->name) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Destino') ?></th>
				                <td><?= h($coMenu->destino) ?></td>
				            </tr>
				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= $this->Number->format($coMenu->id) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Co Menu Id') ?></th>
				                <td><?= $this->Number->format($coMenu->co_menu_id) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Posicion') ?></th>
				                <td><?= $this->Number->format($coMenu->posicion) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($coMenu->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($coMenu->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $coMenu->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="related">
				            <h4><?= __('Related Co Menus') ?></h4>
				            <?php if (!empty($coMenu->co_menus)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Co Menu Id') ?></th>
				    				                    <th scope="col"><?= __('Icon') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Posicion') ?></th>
				    				                    <th scope="col"><?= __('Destino') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coMenu->co_menus as $coMenus): ?>
				                <tr>
				                    <td><?= h($coMenus->id) ?></td>
				                    <td><?= h($coMenus->co_menu_id) ?></td>
				                    <td><?= h($coMenus->icon) ?></td>
				                    <td><?= h($coMenus->name) ?></td>
				                    <td><?= h($coMenus->posicion) ?></td>
				                    <td><?= h($coMenus->destino) ?></td>
				                    <td><?= h($coMenus->activo) ?></td>
				                    <td><?= h($coMenus->created) ?></td>
				                    <td><?= h($coMenus->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoMenus', 'action' => 'view', $coMenus->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoMenus', 'action' => 'edit', $coMenus->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoMenus', 'action' => 'delete', $coMenus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coMenus->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				    				        <div class="related">
				            <h4><?= __('Related Co Grupos') ?></h4>
				            <?php if (!empty($coMenu->co_grupos)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Pagina Inicial') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Notificacion') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coMenu->co_grupos as $coGrupos): ?>
				                <tr>
				                    <td><?= h($coGrupos->id) ?></td>
				                    <td><?= h($coGrupos->name) ?></td>
				                    <td><?= h($coGrupos->pagina_inicial) ?></td>
				                    <td><?= h($coGrupos->activo) ?></td>
				                    <td><?= h($coGrupos->notificacion) ?></td>
				                    <td><?= h($coGrupos->created) ?></td>
				                    <td><?= h($coGrupos->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoGrupos', 'action' => 'view', $coGrupos->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoGrupos', 'action' => 'edit', $coGrupos->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoGrupos', 'action' => 'delete', $coGrupos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coGrupos->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
