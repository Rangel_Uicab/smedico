<style>
       .login-form {
           width: 25rem;
           height: 18.75rem;
           position: fixed;
           top: 50%;
           margin-top: -9.375rem;
           left: 50%;
           margin-left: -12.5rem;
           background-color: #ffffff;
           opacity: 0;
           -webkit-transform: scale(.8);
           transform: scale(.8);
       }
       .hidden {
         display: none !important;
       }
   </style>
<div class="page-login-main">
    <div class="brand hidden-md-up">
      <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo.png" alt="...">
      <h3 class="brand-text font-size-40">SEGUIMIENTO M&Eacute;DICO</h3>
    </div> 
    <h3 class="font-size-24">INICIAR SESI&Oacute;N</h3>
          <?php
            $templatesUsername = [
                                    'input' => '
                                                <div class="form-group form-material floating" data-plugin="formMaterial">                                
                                                    <input autocomplete = "off" class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>
                                                    <label  class="floating-label">Usuario</label>
                                                </div>',
                                    'inputContainer' => '{{content}}',
                                ];


            $myTemplatesPassword = [
                                    'input' => '
                                                <div class="form-group form-material floating" data-plugin="formMaterial">
                                                 <input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>
                                                    <label class="floating-label">Clave de Acceso</label>
                                                       
                                                </div>',
                                    'inputContainer' => '{{content}}',
                                    ];
            ?>
          
           <?= $this->Form->create(null,['role'=>'form','autocomplete'=>"off"]) ?>
             <?php 
                echo $this->Form->input('login',
                                                [
                                                'type'=>'text',
                                                'label'=>false,
                                                'templates'=>$templatesUsername,
                                                ]
                                            );
                echo $this->Form->input('password',
                                                [
                                                    'type'=>'password',
                                                    'label'=>false,
                                                    'templates'=> $myTemplatesPassword
                                                ]
                                       );
                echo $this->Form->button(__('Entrar'),['class'=>'btn btn-primary btn-block btn-lg mt-40']);
                echo $this->Flash->render('flash');
            ?>
           <?= $this->Form->end()?>
      <footer class="page-copyright">
          <p>SESA</p>
          <p>© <?php echo date('Y')?></p>
          <div class="social">
            <a class="btn btn-icon btn-round social-twitter mx-5" target="_blank" href="https://www.twitter.com/sesa_qroo">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-round social-facebook mx-5" target="_blank" href="https://www.facebook.com/SesaQroo">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
    </footer>
</div>
<script type="text/javascript">
    var Login = document.getElementById("login").value;
    if(Login == "")
    {
        document.getElementById("login").focus();
    }
    else
    {
        document.getElementById("password").focus();
    }
</script>
 <script type="text/javascript">
      $(function(){
             var form = $(".login-form");
             form.css({
                 opacity: 0.6,
                 "-webkit-transform": "scale(1)",
                 "transform": "scale(1)",
                 "-webkit-transition": ".5s",
                 "transition": ".5s"
             });
         });
</script>