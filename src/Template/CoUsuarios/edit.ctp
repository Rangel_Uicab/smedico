<?php 
    echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
      echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>

<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>

<div class="row">
    <div class="col-md-3">
        <div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
            <?php echo $this->Html->link('<i class="ti-menu"></i>&nbsp; Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
        </div>
    </div>

    <div class="col-md-9">
        <div class="bgc-white p-10 bd">
        <div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h3 class="panel-title">Actualizar Usuario</h3>
            </div>
            <div class="panel-body">
                <?php echo  $this->Form->create($coUsuario,['role'=>'form','class'=>'form','onsubmit'=>'return checkSubmit();']) ?>
                 <div class="row">
                    <div class="col-md-12"><?php  echo $this->Form->input('cat_municipio_id', ['required'=>true,'empty'=>true,'options' => $catMunicipios,'label'=>['text'=>'Municipio']]);?></div>
                    <div class="col-md-6"><?php  echo $this->Form->input('cat_unidade_id', ['required'=>true,'empty'=>true,'options' => $catUnidades,'label'=>['text'=>'Unidad']]);?></div>
                    <div class="col-md-6"><?php  echo $this->Form->input('cat_area_id', ['required'=>true,'empty'=>true,'label'=>['text'=>'Area']]);?></div>
                </div>
               
                <?php
                  echo $this->Form->control('nombre', ['label'=>['text'=>'Nombre']]);
                  echo $this->Form->control('paterno', ['label'=>['text'=>'Apellido Paterno']]);
                  echo $this->Form->control('materno', ['label'=>['text'=>'Apellido Materno']]);
                  echo $this->Form->control('login', ['label'=>['text'=>'Nombre de Usuario']]);
                ?>
                <?php echo $this->Form->control('password', ['label'=>['text'=>'Clave de Acceso']]);?>
                <?php  echo $this->Form->input('co_grupos._ids', ['options' => $coGrupos,'label'=>['text'=>'Grupo(s)']]);?>
                <?php echo $this->Form->control('activo',['label'=>['text'=>' Activo'],'escape'=>false]); ?>
                <?php echo $this->Form->button('Guardar',['id'=>'btnGuardar','class'=>'btn btn-primary waves-effect']) ?>
                <?php echo $this->Form->end() ?>
            </div>
        </div>
        </div>
    </div>
</div>

<script type="text/javascript">
      $("#co-grupos-ids").select2
      (
      {
          placeholder: "SELECCIONAR",
          allowClear: true,
        width:'100%'
      }
      );
      $("#cat-unidade-id").on('change',function()
      {
        var id = $(this).val();
        $("#cat-area-id").find('option').remove();
     
        if (id)
        {
    //      document.getElementById('cargando').style.display='block';
          var dataString = 'id='+ id;
          $.ajax(
            {
                type: "POST",
                url: "<?php echo $this->Url->build(['controller'=>'cat_areas','action' => 'get_areas_por_unidad']); ?>" ,
                dataType: "json",
                data: dataString,
                cache: false,
                success: function(html)
                {
    //              document.getElementById('cargando').style.display='none';
                  $('<option>').val('').text('Seleccione un Area').appendTo($("#cat-area-id"));
                  $.each(html, function(key, value)
                  {
                    $('<option>').val(key).text(value).appendTo($("#cat-area-id"));
                  });
                }
            });
        }
      });
</script>