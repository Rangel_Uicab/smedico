<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<div class="row gap-20">
	<div class="col-md-12">
        <div class="bgc-white p-10 bd">
          <div class="col-md-12" style="text-align: center;">
          <h3>Busqueda de Expedientes</h3>
          </div>
        <?= $this->Form->create(null,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        <div class="row justify-content-center">
            <div class="col-4">
              <?php echo $this->Form->control('folio', ['placeholder'=>'Ingresar Numero de Folio','escape'=>false,'required' => true,'label'=>['text'=>'']]);?>
            </div>
            <div class="col-2"><br>
            <?= $this->Form->button('<i class="ti-search" aria-hidden="true"></i> Buscar',array('type'=>'submit','class'=>'btn btn-block btn-primary','id'=>'btnGuardar')) ?>
            </div>
            
             <?php
                if($this->request->session()->check('FolioExpediente'))
                {
                ?>
                    <div class="col-2"><br>
                    <?php
                        echo $this->Html->link('<i class="ti-trash"></i> Limpiar', ['action' => 'delete_filtro'],['class'=>'btn btn-block btn-danger','escape'=>false]);
                    ?>
                    </div>
                <?php
                }
             ?>
           

        </div>
        <?= $this->Form->end() ?>

	 <?php
        if($this->request->session()->check('FolioExpediente'))
        {
            if(!empty($opeExpediente))
            { 
        ?>
           <div class="col-md-12">
                <div class="bgc-white p-10 bd">
                    <div class="panel panel-primary panel-line">
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body">
                             PACIENTE: <h3><?= h($opeExpediente->cat_persona->nombre_completo) ?></h3>
                            <table class="table table-sm table-condensed table-bordered">
                                <tr>
                                    <td colspan="2">
                                        <span>
                                            <b>SEXO: </b><?= $opeExpediente->cat_persona->sexo =='H' ? 'HOMBRE' : 'MUJER' ?>
                                        </span>
                                        <br>
                                        <span>
                                            <b>FECHA DE NACIMIENTO: </b><?= date('d-m-Y',strtotime($opeExpediente->cat_persona->fecha_nacimiento)) ?>
                                        </span>
                                        <br> 
                                        <span>
                                            <b>TEL. MOVIL: </b>
                                            <?= 
                                                (!empty($opeExpediente->cat_persona->tel_movil)) ?  $opeExpediente->cat_persona->tel_movil : 'S/N'
                                            ?> 
                                            <b>TEL. CASA: </b> 
                                            <?= h($opeExpediente->cat_persona->tel_casa) ?  $opeExpediente->cat_persona->tel_casa : 'S/N' 
                                            ?>
                                        </span>
                                        <br>
                                    </td>
                                </tr>
                                <tr class="table-active">
                                    <th>EXPEDIENTE</th>
                                    <th>AFILIACI&Oacute;N SEGURO POPULAR</th>
                                </tr>
                                <tr>
                                    <td><?= (!empty($opeExpediente->cat_persona->num_expediente)) ? $opeExpediente->cat_persona->num_expediente : 'S/N' ?></td>
                                    <td><?= (!empty($opeExpediente->cat_persona->num_afiliacion)) ? $opeExpediente->cat_persona->num_afiliacion : 'S/N' ?></td>
                                </tr>
                            </table>
                            <h4>SEGUIMIENTO MEDICO</h4>
                            
                            <table class="table table-sm table-condensed table-bordered">
                                <tr>
                                    <td>
                                        <b>TIPO DE ATENCI&Oacute;N: </b>
                                        <?= $opeExpediente->has('cat_atencione') ? $opeExpediente->cat_atencione->name : '' ?><br>
                                        
                                        <b>MOTIVO: </b>
                                        <?= $opeExpediente->motivo; ?><br> 
                                        
                                        <b>FECHA Y HORA DE INGRESO: </b>
                                        <?php
                                            echo date('Y-m-d',strtotime($opeExpediente->fecha)).' - '.date('H:i:s a',strtotime($opeExpediente->hora));
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            
                            <table id="opeExpediente" class="table table-sm table-striped table-bordered">
                                <tr class="table-primary">
                                    <th><?= __('AREA') ?></th>
                                    <th><?= __('RESPONSABLE') ?></th>
                                    <th><?= __('ESTADO DE SALUD') ?></th>
                                    <th><?= __('F. CREACI&Oacute;N') ?></th>
                                </tr>
                                <?php if (!empty($opeExpediente->ope_seguimientos_expedientes)): ?>

                                <?php foreach ($opeExpediente->ope_seguimientos_expedientes as $opeSeguimientosExpedientes): ?>
                                <tr>
                                    <td><?= ($opeSeguimientosExpedientes->has('cat_area')) ? $opeSeguimientosExpedientes->cat_area->name : '' ?></td>
                                    <td><?= ($opeSeguimientosExpedientes->has('co_usuario')) ? $opeSeguimientosExpedientes->co_usuario->nombre_completo : '' ?></td>
                                    <td><?= ($opeSeguimientosExpedientes->has('cat_estatus')) ? '<span class="'.$opeSeguimientosExpedientes->cat_estatus->clase.'">'.$opeSeguimientosExpedientes->cat_estatus->name.'</span>' : '' ?></td>
                                    <td><?= date('d-m-Y H:i a',strtotime($opeSeguimientosExpedientes->created)) ?></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                     <b>OBSERVACI&Oacute;N: </b><br>
                                     <?= h($opeSeguimientosExpedientes->observacion) ?>
                                    </td>
                                </tr>
                                
                                <?php endforeach; ?>
                                <?php 
                                    else:
                                 ?>
                                        <tr>
                                            <td colspan="4">SIN SEGUIMIENTOS</td>
                                        </tr>
                                 <?php   
                                    endif;
                                 ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            }
            else
            {
                ?>
                <div class="alert alert-danger" role="alert">
                  <strong>Folio no encontrado!</strong> Verifica que el folio ingresado sea el correcto.
                </div>
                <?php
            }
        }  
        ?>   
	         
        </div> 
	</div> 
 </div> 
