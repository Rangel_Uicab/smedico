<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $coGrupo->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $coGrupo->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $coGrupo->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($coGrupo->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($coGrupo->name) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Pagina Inicial') ?></th>
				                <td><?= h($coGrupo->pagina_inicial) ?></td>
				            </tr>
				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= $this->Number->format($coGrupo->id) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($coGrupo->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($coGrupo->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $coGrupo->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Notificacion') ?></th>
				                <td><?= $coGrupo->notificacion ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="related">
				            <h4><?= __('Related Co Menus') ?></h4>
				            <?php if (!empty($coGrupo->co_menus)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Co Menu Id') ?></th>
				    				                    <th scope="col"><?= __('Icon') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Posicion') ?></th>
				    				                    <th scope="col"><?= __('Destino') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coGrupo->co_menus as $coMenus): ?>
				                <tr>
				                    <td><?= h($coMenus->id) ?></td>
				                    <td><?= h($coMenus->co_menu_id) ?></td>
				                    <td><?= h($coMenus->icon) ?></td>
				                    <td><?= h($coMenus->name) ?></td>
				                    <td><?= h($coMenus->posicion) ?></td>
				                    <td><?= h($coMenus->destino) ?></td>
				                    <td><?= h($coMenus->activo) ?></td>
				                    <td><?= h($coMenus->created) ?></td>
				                    <td><?= h($coMenus->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoMenus', 'action' => 'view', $coMenus->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoMenus', 'action' => 'edit', $coMenus->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoMenus', 'action' => 'delete', $coMenus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coMenus->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				    				        <div class="related">
				            <h4><?= __('Related Co Permisos') ?></h4>
				            <?php if (!empty($coGrupo->co_permisos)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Descripcion') ?></th>
				    				                    <th scope="col"><?= __('Controller') ?></th>
				    				                    <th scope="col"><?= __('Action') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coGrupo->co_permisos as $coPermisos): ?>
				                <tr>
				                    <td><?= h($coPermisos->id) ?></td>
				                    <td><?= h($coPermisos->name) ?></td>
				                    <td><?= h($coPermisos->descripcion) ?></td>
				                    <td><?= h($coPermisos->controller) ?></td>
				                    <td><?= h($coPermisos->action) ?></td>
				                    <td><?= h($coPermisos->activo) ?></td>
				                    <td><?= h($coPermisos->created) ?></td>
				                    <td><?= h($coPermisos->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoPermisos', 'action' => 'view', $coPermisos->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoPermisos', 'action' => 'edit', $coPermisos->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoPermisos', 'action' => 'delete', $coPermisos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coPermisos->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				    				        <div class="related">
				            <h4><?= __('Related Co Usuarios') ?></h4>
				            <?php if (!empty($coGrupo->co_usuarios)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Nivel Academico') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Id') ?></th>
				    				                    <th scope="col"><?= __('Nombre') ?></th>
				    				                    <th scope="col"><?= __('Paterno') ?></th>
				    				                    <th scope="col"><?= __('Materno') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Login') ?></th>
				    				                    <th scope="col"><?= __('Password') ?></th>
				    				                    <th scope="col"><?= __('Ultimo Acceso') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coGrupo->co_usuarios as $coUsuarios): ?>
				                <tr>
				                    <td><?= h($coUsuarios->id) ?></td>
				                    <td><?= h($coUsuarios->nivel_academico) ?></td>
				                    <td><?= h($coUsuarios->cat_unidade_id) ?></td>
				                    <td><?= h($coUsuarios->nombre) ?></td>
				                    <td><?= h($coUsuarios->paterno) ?></td>
				                    <td><?= h($coUsuarios->materno) ?></td>
				                    <td><?= h($coUsuarios->activo) ?></td>
				                    <td><?= h($coUsuarios->login) ?></td>
				                    <td><?= h($coUsuarios->password) ?></td>
				                    <td><?= h($coUsuarios->ultimo_acceso) ?></td>
				                    <td><?= h($coUsuarios->created) ?></td>
				                    <td><?= h($coUsuarios->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoUsuarios', 'action' => 'view', $coUsuarios->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoUsuarios', 'action' => 'edit', $coUsuarios->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoUsuarios', 'action' => 'delete', $coUsuarios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coUsuarios->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
