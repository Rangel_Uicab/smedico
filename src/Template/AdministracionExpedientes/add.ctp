<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php 
//	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
//  echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<div class="row gap-20">
	<div class="col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
					</div>
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
            <div class="card border-light mb-3">
	            <div class="card-header">
	                 <h4 class="c-grey-900 mB-20"> <?= __('Add Ope Expediente') ?></h4>
	            </div>
	            <div class="card-body">
                <p style="text-align:rigth;"> <h5 class="card-title">(<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>) Campos obligatorios</h5></p>

        		    <?= $this->Form->create($opeExpediente,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        	        			    
				    <?php
                    		    echo $this->Form->control('co_usuario_id', ['escape'=>false,'required' => true,'options' => $coUsuarios,'label'=>['text'=>'co_usuario_id <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                                echo $this->Form->control('cat_unidade_id', ['escape'=>false,'required' => true,'label'=>['text'=>'cat_unidade_id <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                                        echo $this->Form->control('cat_area_id', ['escape'=>false,'required' => true,'label'=>['text'=>'cat_area_id <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                            		    echo $this->Form->control('cat_persona_id', ['escape'=>false,'required' => true,'options' => $catPersonas,'label'=>['text'=>'cat_persona_id <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                    		    echo $this->Form->control('cat_atencione_id', ['escape'=>false,'required' => true,'options' => $catAtenciones,'label'=>['text'=>'cat_atencione_id <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                    		    echo $this->Form->control('cat_tipos_alta_id', ['options' => $catTiposAltas, 'empty' => true,'label'=>[]]);
                    		    echo $this->Form->control('cat_unidade_traslado_id', ['options' => $catUnidades, 'empty' => true,'label'=>[]]);
                                                echo $this->Form->control('motivo', ['escape'=>false,'required' => true,'label'=>['text'=>'motivo <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                                        echo $this->Form->control('fecha', ['escape'=>false,'required' => true,'label'=>['text'=>'fecha <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                                        echo $this->Form->control('hora', ['escape'=>false,'required' => true,'label'=>['text'=>'hora <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                                                        echo $this->Form->control('observacion', ['label'=>[]]);
                                                        echo $this->Form->control('observacion_alta', ['label'=>[]]);
                            		    echo $this->Form->control('fecha_alta', ['empty' => true,'label'=>['']]);
                    		    echo $this->Form->control('hora_alta', ['empty' => true,'label'=>['']]);
        	            ?>
       			    <?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>

        		    <?= $this->Form->end() ?>
    		    </div>
            </div>
	    </div>
        
	</div>
</div>

<script type="text/javascript">
//   	$("#Example-id").select2	({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});	
</script>