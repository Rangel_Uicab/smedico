<?php
  echo $this->Html->css(
                        array(
                                'datatables_b4/responsive.bootstrap4.min'
                             )
                        );
  echo $this->Html->script(
                            array(
                                    'datatables_b4/jquery.dataTables.min',
                                    'datatables_b4/dataTables.responsive.min',
                                    'datatables_b4/responsive.bootstrap4.min'
                                 )
                            ); 
?>

<div class="row gap-20">
	<div class="col-md-12">
        <div class="bgc-white p-10 bd"> 
	       <div class="col-md-12">
               <div class="text-center">
	             <h2>Administraci&oacute;n de Expedientes</h2>        
	           </div> 
	            <br>
	        </div>
	    
	        <div class="col-md-12">
	 	        <table id="opeExpediente" class="table table-responsive w-100 d-block d-md-table table-striped">
	                <thead>                         
                         <tr>
	        	            <th>Unidad</th>
	        	            <th>Area</th>
	        	            <th>Paciente</th>
	        	            <th>Tipo Atenci&oacute;n</th>
	        	            <th>Motivo</th>
	        	            <th>F. Ingreso</th>
                            <th>Alta</th>
	        	            <th>Folio</th>
	        	            <th>Acciones</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <tr>
	                        <td colspan="9">Cargando datos...</td>
	                    </tr>
	                </tbody>
	            </table>
            </div> 
        </div> 
	</div> 
 </div> 
 
<script type="text/javascript" charset="utf-8">
	$.extend(true, $.fn.dataTable.defaults, 
    { 
    	lengthMenu: [[10, 25, 50, 250, 999999], [10, 25, 50, 250, "Todos"]]
	});
	var tbl = $('#opeExpediente');

    var tabla = $('#opeExpediente').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            'responsive': true,
            "sAjaxSource": "<?php echo $this->Url->build(array('action'=>'get_data','_ext'=>'json')); ?>", 
//			'dom': 'Bfrtip',	
            "language": {
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords": "Sin registros",
                        "sInfo": "Mostrando registros de _START_ a _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Sin registros",
                        "sInfoFiltered": "",//"(Mostrando _MAX_ registros por pagina)",
                        "oPaginate": {
                            "sFirst": "Inicio",
                            "sLast": " Final",
                            "sNext": ">",
                            "sPrevious": "<"
                        },
                        "sSearch": " ",
                        "sSearchPlaceholder": "Buscar...",
                        "sProcessing":"Cargando"
                    },
                    "aoColumnDefs": [
                                            { "bSortable": false, "aTargets": [8] }
                                    ]
        });
     
</script>