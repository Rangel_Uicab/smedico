<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $opeExpediente->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $opeExpediente->id],['confirm' => __('Realmente desea eliminar el Expediente del Paciente {0}?', $opeExpediente->cat_persona->nombre_completo),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n del Expediente</h2>
                </div>
                <div class="panel-body">
             	    PACIENTE: <br>    
                    <h3><?= h($opeExpediente->has('cat_persona') ? $opeExpediente->cat_persona->nombre_completo : '') ?></h3>
                    <hr>
                    <b>Unidad de Registro: </b> <?= h($opeExpediente->has('cat_unidade') ? $opeExpediente->cat_unidade->name : '') ?><br>
                    <b>Area de Registro: </b> <?= h($opeExpediente->has('cat_area') ? $opeExpediente->cat_area->name : '') ?><br>
                    <b>Usuario Registro: </b> <?= h($opeExpediente->has('co_usuario') ? $opeExpediente->co_usuario->nombre_completo : '') ?><br>
                    <hr>
                    <h2>INGRESO DEL PACIENTE</h2>
                    <b>Tipo de Atenci&oacute;n: </b> <?= h($opeExpediente->has('cat_atencione') ? $opeExpediente->cat_atencione->name : '') ?><br>
                    <b>Motivo Atenci&oacute;n: </b> <?= $opeExpediente->motivo?><br>
                    <b>Fecha y Hora Ingreso: </b> <?=  date_format($opeExpediente->fecha,'d-m-Y'). ' '.date_format($opeExpediente->hora,'H:i a')?><br>
                    
                    <h2>ALTA DEL PACIENTE</h2>
                    <b>Tipo de Alta: </b> <?= h($opeExpediente->has('cat_tipos_alta') ? $opeExpediente->cat_tipos_alta->name : 'NO SE HA DADO DE ALTA AL PACIENTE') ?><br>
                    <b>Observaci&oacute;n: </b> <?= $opeExpediente->observacion_alta?><br>
                    <b>Fecha y Hora Alta: </b> 
                    <?php  
                        if($opeExpediente->has('cat_tipos_alta'))
                        {
                            echo date_format($opeExpediente->fecha_alta,'d-m-Y'). ' '.date_format($opeExpediente->hora_alta,'H:i a');  
                        }
                        else
                        {
                            echo 'NO SE HA DADO DE ALTA AL PACIENTE';   
                        }
                    ?>
                    <hr>
                    REGISTRO EN EL SISTEMA <br>
                    <b>F. Creaci&oacute;n: </b> <?php echo date_format($opeExpediente->created,'d-m-Y H:i a');  ?><br>
                    <b>F. Ultima Modificaci&oacute;n: </b> <?php echo date_format($opeExpediente->modified,'d-m-Y H:i a');  ?>

                    <hr>
				    <div class="related">
				            <h4><?= __('Seguimientos del Expediente') ?></h4>
				            <?php if (!empty($opeExpediente->ope_seguimientos_expedientes)) 
                            {
                                
                            ?>
                            <table class="table table-sm table-condensed table-bordered">
                                <tr class="table-primary">
				    				<th><?= __('AREA') ?></th>
                                    <th><?= __('RESPONSABLE') ?></th>
                                    <th><?= __('ESTADO DE SALUD') ?></th>
                                    <th><?= __('F. CREACI&Oacute;N') ?></th>
				                </tr>
				                <?php foreach ($opeExpediente->ope_seguimientos_expedientes as $opeSeguimientosExpedientes): ?>
				                <tr>
				                    <td><?= ($opeSeguimientosExpedientes->has('cat_area')) ? $opeSeguimientosExpedientes->cat_area->name : '' ?></td>
                                    <td><?= ($opeSeguimientosExpedientes->has('co_usuario')) ? $opeSeguimientosExpedientes->co_usuario->nombre_completo : '' ?></td>
                                    <td><?= ($opeSeguimientosExpedientes->has('cat_estatus')) ? '<span class="'.$opeSeguimientosExpedientes->cat_estatus->clase.'">'.$opeSeguimientosExpedientes->cat_estatus->name.'</span>' : '' ?></td>
                                    <td><?= date('d-m-Y H:i a',strtotime($opeSeguimientosExpedientes->created)) ?></td>
				                </tr>
                                <?php 
                                    endforeach;  
                            }
                            else
                            {
                                ?>
                                <tr>
                                    <td colspan="4">SIN SEGUIMIENTOS</td>
                                </tr>
                                <?php
                            }
                             ?>
                             </table>
				    </div>
				</div>
            </div>
        </div>
	</div>
</div>
