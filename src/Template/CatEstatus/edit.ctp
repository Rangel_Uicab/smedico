<?php
/**
  * @var \App\View\AppView $this
  */
?>
<script type="text/javascript">
var statSend = false;
function checkSubmit() 
{   
    if (!statSend) 
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    } 
    else 
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php 
//	echo $this->Html->css('select2/select2.minfd53',['block'=>true]);
//  echo $this->Html->script('select2/select2.full.minfd53',['block'=>true]);
?>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>
<div class="row gap-20">
	<div class="col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			            	<?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catEstatus->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catEstatus->id),'escape'=>false,'class'=>'list-group-item'])?>
        			</div>
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
            <div class="card border-light mb-3">
	            <div class="card-header">
	                 <h4 class="c-grey-900 mB-20"> <?= __('Edit Cat Estatus') ?></h4>
	            </div>
	            <div class="card-body">
                <p style="text-align:rigth;"> <h5 class="card-title">(<span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>) Campos obligatorios</h5></p>

        		    <?= $this->Form->create($catEstatus,['role'=>'form','onsubmit'=>'return checkSubmit();']) ?>
        	        			    
				    <?php
                                                echo $this->Form->control('name', ['escape'=>false,'required' => true,'label'=>['text'=>'name <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">*</span>']]);
                
            		   // echo $this->Form->control('name', ['label'=>[]]);
                                            echo $this->Form->control('editar', ['label'=>[]]);
                
            		   // echo $this->Form->control('editar', ['label'=>[]]);
                                            echo $this->Form->control('clase', ['label'=>[]]);
                
            		   // echo $this->Form->control('clase', ['label'=>[]]);
    	            ?>
       			    <?= $this->Form->button('Guardar',array('type'=>'submit','class'=>'btn btn-primary waves-effect','id'=>'btnGuardar')) ?>

        		    <?= $this->Form->end() ?>
    		    </div>
            </div>
	    </div>
        
	</div>
</div>

<script type="text/javascript">
//   	$("#Example-id").select2	({placeholder: "SELECCIONAR",allowClear: true, width:'100%'});	
</script>