<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catAtencione->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catAtencione->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catAtencione->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catAtencione->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= h($catAtencione->id) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($catAtencione->name) ?></td>
				            </tr>
				    				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catAtencione->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($catAtencione->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $catAtencione->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="related">
				            <h4><?= __('Related Ope Expedientes') ?></h4>
				            <?php if (!empty($catAtencione->ope_expedientes)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Co Usuario Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Persona Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Atencione Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Tipos Alta Id') ?></th>
				    				                    <th scope="col"><?= __('Cat Unidade Traslado Id') ?></th>
				    				                    <th scope="col"><?= __('Motivo') ?></th>
				    				                    <th scope="col"><?= __('Fecha') ?></th>
				    				                    <th scope="col"><?= __('Hora') ?></th>
				    				                    <th scope="col"><?= __('Observacion') ?></th>
				    				                    <th scope="col"><?= __('Observacion Alta') ?></th>
				    				                    <th scope="col"><?= __('Fecha Alta') ?></th>
				    				                    <th scope="col"><?= __('Hora Alta') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($catAtencione->ope_expedientes as $opeExpedientes): ?>
				                <tr>
				                    <td><?= h($opeExpedientes->id) ?></td>
				                    <td><?= h($opeExpedientes->co_usuario_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_persona_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_atencione_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_tipos_alta_id) ?></td>
				                    <td><?= h($opeExpedientes->cat_unidade_traslado_id) ?></td>
				                    <td><?= h($opeExpedientes->motivo) ?></td>
				                    <td><?= h($opeExpedientes->fecha) ?></td>
				                    <td><?= h($opeExpedientes->hora) ?></td>
				                    <td><?= h($opeExpedientes->observacion) ?></td>
				                    <td><?= h($opeExpedientes->observacion_alta) ?></td>
				                    <td><?= h($opeExpedientes->fecha_alta) ?></td>
				                    <td><?= h($opeExpedientes->hora_alta) ?></td>
				                    <td><?= h($opeExpedientes->created) ?></td>
				                    <td><?= h($opeExpedientes->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'OpeExpedientes', 'action' => 'view', $opeExpedientes->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'OpeExpedientes', 'action' => 'edit', $opeExpedientes->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'OpeExpedientes', 'action' => 'delete', $opeExpedientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $opeExpedientes->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
