<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $coPermiso->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $coPermiso->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $coPermiso->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($coPermiso->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    				            <tr>
				                <th scope="row"><?= __('Name') ?></th>
				                <td><?= h($coPermiso->name) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Controller') ?></th>
				                <td><?= h($coPermiso->controller) ?></td>
				            </tr>
				    				    				    				            <tr>
				                <th scope="row"><?= __('Action') ?></th>
				                <td><?= h($coPermiso->action) ?></td>
				            </tr>
				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= $this->Number->format($coPermiso->id) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($coPermiso->created) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Modified') ?></th>
				                <td><?= h($coPermiso->modified) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Activo') ?></th>
				                <td><?= $coPermiso->activo ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>'; ?></td>
				            </tr>
				    				    				        </table>
				    				    				        <div class="row">
				            <h4><?= __('Descripcion') ?></h4>
				            <?= $this->Text->autoParagraph(h($coPermiso->descripcion)); ?>
				        </div>
				    				    				    				        <div class="related">
				            <h4><?= __('Related Co Grupos') ?></h4>
				            <?php if (!empty($coPermiso->co_grupos)): ?>
				            <table class="table table-bordered" cellpadding="0" cellspacing="0">
				                <tr>
				    				                    <th scope="col"><?= __('Id') ?></th>
				    				                    <th scope="col"><?= __('Name') ?></th>
				    				                    <th scope="col"><?= __('Pagina Inicial') ?></th>
				    				                    <th scope="col"><?= __('Activo') ?></th>
				    				                    <th scope="col"><?= __('Notificacion') ?></th>
				    				                    <th scope="col"><?= __('Created') ?></th>
				    				                    <th scope="col"><?= __('Modified') ?></th>
				    				                    <th scope="col" class="actions"><?= __('Actions') ?></th>
				                </tr>
				                <?php foreach ($coPermiso->co_grupos as $coGrupos): ?>
				                <tr>
				                    <td><?= h($coGrupos->id) ?></td>
				                    <td><?= h($coGrupos->name) ?></td>
				                    <td><?= h($coGrupos->pagina_inicial) ?></td>
				                    <td><?= h($coGrupos->activo) ?></td>
				                    <td><?= h($coGrupos->notificacion) ?></td>
				                    <td><?= h($coGrupos->created) ?></td>
				                    <td><?= h($coGrupos->modified) ?></td>
				                    <td class="actions">
				                        <?= $this->Html->link(__('View'), ['controller' => 'CoGrupos', 'action' => 'view', $coGrupos->id]) ?>
				                        <?= $this->Html->link(__('Edit'), ['controller' => 'CoGrupos', 'action' => 'edit', $coGrupos->id]) ?>
				                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'CoGrupos', 'action' => 'delete', $coGrupos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coGrupos->id)]) ?>
				                    </td>
				                </tr>
				                <?php endforeach; ?>
				            </table>
				            <?php endif; ?>
				        </div>
				                    </div>
            </div>
        </div>
	</div>
</div>
