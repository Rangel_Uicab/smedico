<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CONSULTA DE PACIENTES:';
?>
<!DOCTYPE html>
<html class="no-js css-menubar">
<head>
    <?= $this->Html->charset() ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <style>
        #loader{transition:all .3s ease-in-out;opacity:1;visibility:visible;position:fixed;height:100vh;width:100%;background:#fff;z-index:90000}
        #loader.fadeOut{opacity:0;visibility:hidden}.spinner{width:40px;height:40px;position:absolute;top:calc(50% - 20px);left:calc(50% - 20px);background-color:#333;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}100%{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}
    </style>

    <?php echo $this->Html->css(
									[
									 'adminator/style'
									]
								)
    ?>
    <?php
    echo $this->Html->script(
                                [
                                    'jquery/jquery.minfd53.js?v4.0.1'
                                ]
                                );
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>

    <script type="text/javascript">
        window.addEventListener('load', () =>
        {
        const loader = document.getElementById('loader');
        setTimeout(() => {
          loader.classList.add('fadeOut');
        }, 100);
      });
    </script>

    <style>
    .main-content {padding: 20px 20px 20px;}
    </style>

    <main class="main-content bgc-white-100">
        <div id="mainContent">
            <?= $this->Flash->render()?>
            <?= $this->fetch('content') ?>
       </div>
    </main>
    <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
        <span>SISTEMA DE SEGUIMIENTO DE PACIENTES © <?php echo date('Y')?> </span>
    </footer>
      <?php
        echo $this->Html->script(
                                    [
                                        'adminator/vendor',
                                        'adminator/bundle',
                                        'bootstrap-notify.min'
                                    ]
                                    );
      ?>
</body>

</html>
