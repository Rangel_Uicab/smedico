<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html class="no-js css-menubar">
<head>
    <?= $this->Html->charset() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>

        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php echo $this->Html->css(
                                    [
                                     'bootstrap/bootstrap.minfd53.css?v4.0.1',
                                     'bootstrap/bootstrap-extend.minfd53.css?v4.0.1',
                                     'core/site.minfd53.css?v4.0.1',
                                     'login-v2.minfd53.css?v4.0.1',
                                     'material-design/material-design.minfd53.css?v4.0.1',
                                     'brand-icons/brand-icons.minfd53.css?v4.0.1',         
                                    ]
                                ) 
    ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="animsition page-login-v2 layout-full page-dark">

<div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
      <div class="page-brand-info">
        <div class="brand">
          <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo.png" alt="...">
          <h2 class="brand-text font-size-40">SEGUIMIENTO M&Eacute;DICO</h2>
        </div>
        <p class="font-size-20">SERVICIOS ESTATALES DE SALUD</p>
      </div>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
  </div>
  
 <?php
         echo $this->Html->script(
                                    [   
                                        'jquery/jquery.minfd53.js?v4.0.1',
//                                        'core/v1.minfd53.js?v4.0.1',                                    
                                    ]
                                ); 
 ?>
</body>
</html>
