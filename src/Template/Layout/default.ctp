<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'ATENCION PACIENTES:';
?>
<!DOCTYPE html>
<html class="no-js css-menubar">
<head>
    <?= $this->Html->charset() ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
    <style>
        #loader{transition:all .3s ease-in-out;opacity:1;visibility:visible;position:fixed;height:100vh;width:100%;background:#fff;z-index:90000}
        #loader.fadeOut{opacity:0;visibility:hidden}.spinner{width:40px;height:40px;position:absolute;top:calc(50% - 20px);left:calc(50% - 20px);background-color:#333;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}100%{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}
    </style>
    
    <?php echo $this->Html->css(
									[
									 'adminator/style'         
									]
								) 
    ?>
    <?php 
    echo $this->Html->script(
                                [                                    
                                    'jquery/jquery.minfd53.js?v4.0.1',        
                                ]
                                ); 
    ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
</head>
<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    
    <script type="text/javascript">
        window.addEventListener('load', () => 
        {
        const loader = document.getElementById('loader');
        setTimeout(() => {
          loader.classList.add('fadeOut');
        }, 100);
      });
    </script>
      
      <div>
        <div class="sidebar">
            <div class="sidebar-inner">
                <div class="sidebar-logo">
                  <div class="peers ai-c fxw-nw">
                    <div class="peer peer-greed">
                    
                   <?php 
                 /*  echo $this->Html->link(
                                                   '<div class="peers ai-c fxw-nw">
                                                        <div class="peer">
                                                            <div class="logo">
                                                                <img src="'.$this->request->webroot.'logo.jpg'.'" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="peer peer-greed">
                                                            <h5 class="lh-1 mB-0 logo-text">SESAQROO</h5>
                                                        </div>
                                                  </div>'
                                                   ,
                                                    $menu['destino'],
                                                    [
                                                        'class'=>'sidebar-link td-n',
                                                        'escape'=>false
                                                    ]
                                                )   */
                     ?>
                    
                        <a class="sidebar-link td-n" href="index.html" class="td-n">
                          <div class="peers ai-c fxw-nw">
                                <div class="peer">
                                    <div class="logo">
                                        <img src="<?php echo $this->request->webroot.'img/rsz_logo.jpg';?>" alt="">
                                    </div>
                                </div>
                                <div class="peer peer-greed" style="text-align: center;">
                                    <h5 class="lh-1 mB-0 logo-text">SERVICIOS ESTATALES DE SALUD</h5>
                                </div>
                          </div>
                          
                        </a>
                    </div>
              
                    <div class="peer">
                        <div class="mobile-toggle sidebar-toggle">
                            <a href="#" class="td-n"><i class="ti-arrow-circle-left"></i></a>
                        </div>
                    </div>
                  </div>
              </div>
                <ul class="sidebar-menu scrollable pos-r">
                
                    <!--<li class="nav-item mT-30 active">
                        <a class="sidebar-link" href="index.html" default><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Dashboard</span></a>
                    </li>-->
                    <?php
                    if(!empty($menus))
                    {
                        foreach ($menus['CoMenus'] as $menu)
                        {
                            if(empty($menu['menus_hijos']))
                            {
                                ?>
                                
                                <li class="nav-item">                          
                                    <?php echo $this->Html->link(
                                                                    '<span class="icon-holder"><i class="'.$menu['icon'].'" ></i></span>'.'<span class="title">'.$menu['name'].'</span>',
                                                                    $menu['destino'],
                                                                    [
                                                                        'class'=>'sidebar-link',
                                                                        'escape'=>false
                                                                    ]
                                                                )
                                    ?>
                                </li>
                                <?php
                            }
                            else
                            {
                                ?>
                                <li class="nav-item dropdown">
                                    <?php
                                    echo $this->Html->link(
                                                            '<span class="icon-holder"><i class="'.$menu['icon'].'"></i></span>'.'<span class="title">'.$menu['name'].'</span> <span class="arrow"><i class="ti-angle-right"></i></span>',
                                                                'javascript:void(0)',
                                                                [
                                                                    'class'=>'dropdown-toggle',
                                                                    'escape'=>false
                                                                ]
                                                            )
                                    ?>
                                    <ul class="dropdown-menu">
                                        <?php
                                            foreach ($menu['menus_hijos'] as $subMenu)
                                            {
                                                ?>
                                                <li>
                                                    <?php echo $this->Html->link(
                                                                                    $subMenu['name'],
                                                                                    $subMenu['destino'],
                                                                                    [
                                                                                        'class'=>'sidebar-link',
                                                                                        'escape'=>false
                                                                                    ]
                                                                                 );
                                                    ?>
                                                </li>
                                                <?php
                                            }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                    }
                    ?>              
              </ul>
              
            </div>
        </div>
        <div class="page-container">
            <div class="header navbar">
                <div class="header-container">
                    <ul class="nav-left">
                        <li>
                            <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a>
                        </li>
                       
                    </ul>
                    <ul class="nav-right">
                        <li class="notifications dropdown"><span class="counter bgc-red">3</span> 
                            <a href="#" class="dropdown-toggle no-after" data-toggle="dropdown"><i class="ti-bell"></i></a>
                            <ul class="dropdown-menu">
                                <li class="pX-20 pY-15 bdB"><i class="ti-bell pR-10"></i> <span class="fsz-sm fw-600 c-grey-900">Notifications</span></li>
                                <li>
                                    <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                        <li>
                                            <a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                                <div class="peer mR-15">
                                                    <img class="w-3r bdrs-50p" src="../../../randomuser.me/api/portraits/men/1.jpg" alt="">
                                                </div>
                                                <div class="peer peer-greed">
                                                    <span>
                                                        <span class="fw-500">
                                                         <?php
                                                            echo $this->request->Session()->read('Auth.User.nombre_completo');
                                                          ?>
                                                        </span> 
                                                        <span class="c-grey-600">
                                                            liked your 
                                                            <span class="text-dark">post</span>
                                                        </span>
                                                    </span>
                                                    <p class="m-0"><small class="fsz-xs">5 mins ago</small></p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pX-20 pY-15 ta-c bdT">
                                    <span>
                                        <a href="#" class="c-grey-600 cH-blue fsz-sm td-n">View All Notifications <i class="ti-angle-right fsz-xs mL-10"></i></a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li class="notifications dropdown">
                            <span class="counter bgc-blue">3</span> 
                            <a href="#" class="dropdown-toggle no-after" data-toggle="dropdown"><i class="ti-email"></i></a>
                            <ul class="dropdown-menu">
                                <li class="pX-20 pY-15 bdB"><i class="ti-email pR-10"></i> <span class="fsz-sm fw-600 c-grey-900">Emails</span></li>
                                <li>
                                    <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
                                        <li>
                                            <a href="#" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100">
                                                <div class="peer mR-15">
                                                    <img class="w-3r bdrs-50p" src="../../../randomuser.me/api/portraits/men/1.jpg" alt="">
                                                </div>
                                                <div class="peer peer-greed">
                                                    <div>
                                                        <div class="peers jc-sb fxw-nw mB-5">
                                                            <div class="peer">
                                                                <p class="fw-500 mB-0">John Doe</p>
                                                            </div>
                                                            <div class="peer">
                                                                <small class="fsz-xs">5 mins ago</small>
                                                            </div>
                                                        </div>
                                                        <span class="c-grey-600 fsz-sm">Want to create your own customized data generator for your app...</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pX-20 pY-15 ta-c bdT">
                                    <span>
                                        <a href="#" class="c-grey-600 cH-blue fsz-sm td-n">View All Email <i class="fs-xs ti-angle-right mL-10"></i></a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                                <div class="peer mR-10"><img class="w-2r bdrs-50p" src="../../../randomuser.me/api/portraits/men/10.jpg" alt=""></div>
                                <div class="peer"><span class="fsz-sm c-grey-900">
                                <?php
                                    echo $this->request->Session()->read('Auth.User.nombre_completo');
                                  ?>
                                </span></div>
                            </a>
                            <ul class="dropdown-menu fsz-sm">
                                <li>
                                    <a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-settings mR-10"></i> <span>Setting</span></a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <?= $this->Html->link(
                                                            '<i class="ti-power-off mR-10" aria-hidden="true"></i>&nbsp;Cerrar Sesi&oacute;n',
                                                            [
                                                                    'controller'=>'co_usuarios',
                                                                    'action'=>'logout'
                                                            ],
                                                            [
                                                                    'escape'=>false,
                                                                    'class'=>'d-b td-n pY-5 bgcH-grey-100 c-grey-700',
                                                                    'role'=>'menuitem'
                                                            ]
                                                        ) 
                                    ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                   <!-- <div class="row gap-20 masonry pos-r">
                        <div class="masonry-sizer col-md-12"></div>
                        <div class="masonry-item w-100">
                            <div class="row gap-10">
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Total Visits</h6>
                                        </div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed">
                                                    <span id="sparklinedash"></span>
                                                </div>
                                                <div class="peer">
                                                    <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">+10%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Total Page Views</h6>
                                        </div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed">
                                                    <span id="sparklinedash2"></span>
                                                </div>
                                                <div class="peer">
                                                    <span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">-7%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10">
                                            <h6 class="lh-1">Unique Visitor</h6>
                                        </div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash3"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-purple-50 c-purple-500">~12%</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="layers bd bgc-white p-20">
                                        <div class="layer w-100 mB-10"><h6 class="lh-1">Bounce Rate</h6></div>
                                        <div class="layer w-100">
                                            <div class="peers ai-sb fxw-nw">
                                                <div class="peer peer-greed"><span id="sparklinedash4"></span></div>
                                                <div class="peer"><span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">33%</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <?= $this->Flash->render()?>
                    <?= $this->fetch('content') ?>
               </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>SISTEMA DE SEGUIMIENTO DE PACIENTES © <?php echo date('Y')?> 
                </span>
            </footer>
        </div>
      </div>

      <?php 
        echo $this->Html->script(
                                    [                                    
                                        'adminator/vendor',        
                                        'adminator/bundle',        
                                    ]
                                    ); 
      ?>
</body>

</html>
