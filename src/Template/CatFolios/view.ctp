<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row gap-20">
	<div class="masonry-item col-md-3">
		<div class="list-group">
            <a class="list-group-item list-group-item-action active" style="color: #FFFFFF;"><b>Acciones</b></a>
			<?= $this->Html->link('<i class="ti-menu">&nbsp;</i>&nbsp;Listado', ['action' => 'index'],['class'=>'list-group-item','escape'=>false]) ?>
			<?= $this->Html->link('<i class="ti-eye">&nbsp;</i>&nbsp;Editar', ['action' => 'edit', $catFolio->id],['class'=>'list-group-item','escape'=>false]) ?>				
	        <?= $this->Form->postLink(__('<i class="ti-trash">&nbsp;</i>&nbsp;Eliminar'),['action' => 'delete', $catFolio->id],['confirm' => __('Realmente desea eliminar el registro con Id # {0}?', $catFolio->id),'escape'=>false,'class'=>'list-group-item'])?>
	    </div>	
	</div>

	<div class="col-md-9">
        <div class="bgc-white p-10 bd">
		    <div class="panel panel-primary panel-line">
                <div class="panel-heading">
                    <h2 class="panel-title">Informaci&oacute;n</h2>
                </div>
                <div class="panel-body">
             	    <h3><?= h($catFolio->name) ?></h3>
			        <table class="table table-condensed">
				    				    				    		    <tr>
				                <th scope="row"><?= __('Cat Unidade') ?></th>
				                <td><?= $catFolio->has('cat_unidade') ? $this->Html->link($catFolio->cat_unidade->name, ['controller' => 'CatUnidades', 'action' => 'view', $catFolio->cat_unidade->id]) : '' ?></td>
				            </tr>
				    				    				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Id') ?></th>
				                <td><?= $this->Number->format($catFolio->id) ?></td>
				            </tr>
				    				            <tr>
				                <th scope="row"><?= __('Folio') ?></th>
				                <td><?= $this->Number->format($catFolio->folio) ?></td>
				            </tr>
				    				    				    				    				            <tr>
				                <th scope="row"><?= __('Created') ?></th>
				                <td><?= h($catFolio->created) ?></td>
				            </tr>
				    				    				    				        </table>
				    				                    </div>
            </div>
        </div>
	</div>
</div>
