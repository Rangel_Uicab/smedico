<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<?php
echo $this->Html->css(
                            [
                             'animate'
                            ]
                          )
?>

<script type="text/javascript">
     // $('#carouselExampleIndicators').carousel();
</script>
<style>


</style>
<?php

$dias = array(
                1 => "LUNES",
                2 => "MARTES",
                3 => "MIERCOLES",
                4 => "JUEVES",
                5 => "VIERNES",
                6 => "SABADO",
                7 => "DOMINGO",
              );
$Meses = array(
               '01'=>'ENERO',
               '02'=>'FEBRERO',
               '03'=>'MARZO',
               '04'=>'ABRIL',
               '05'=>'MAYO',
               '06'=>'JUNIO',
               '07'=>'JULIO',
               '08'=>'AGOSTO',
               '09'=>'SEPTIEMBRE',
               '10'=>'OCTUBRE',
               '11'=>'NOVIEMBRE',
               '12'=>'DICIEMBRE',
              );
?>
<div class="row">
	<div class="col-md-12">
          <div class="col-md-12" style="text-align: center;">
          <h1 class="text-info">HOSPITAL GENERAL DE PLAYA DEL CARMEN</h1>
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo2.jpg"
              title="Remark">
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo.png"
              title="Remark">
          </div>
          <!-- <div class="col-md-12" style="text-align: center;">
            <h2>BIENVENIDO(A)</h2>
          </div> -->
	</div>
 </div>
 <div class="row">
   <div class="col-md-12" id="DivExpediente">

     <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="1000">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="card">
              <div class="card-header">
                Featured
              </div>
              <div class="card-body">
                <h5 class="card-title">TABLA 1</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="card">
              <div class="card-header">
                Featured
              </div>
              <div class="card-body">
                <h5 class="card-title">TABLA 2</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="card">
              <div class="card-header">
                Featured
              </div>
              <div class="card-body">
                <h5 class="card-title">TABLA 3</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="#" class="btn btn-primary">Go somewhere</a>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div> -->

      <div id="Banner" class="carousel slide" data-ride="carousel" data-interval="5000">
        <ol class="carousel-indicators">
          <?php
            for ($i=1; $i <= $tablas ; $i++)
            {
              $clase = ($i == 1) ? "active" : "";
              ?>
              <li data-target="#Banner" data-slide-to="<?php echo $i ?>" class="<?php echo $clase?>"></li>
              <?php
            }
          ?>
        </ol>

         <div class="carousel-inner">

           <?php
             for ($i=1; $i <= $tablas ; $i++)
             {
               $clase = ($i == 1) ? "carousel-item active" : "carousel-item";
            ?>
                <div class="<?php echo $clase ?>">
                  <div class="card">
                    <div class="card-header text-center bg-primary text-white">
                      <h3>LISTA DE PACIENTES <small>PAGINA <?php echo $i?></small></h3>
                    </div>
                    <div class="card-body">
                      <table class="table table-sm table-striped table-bordered">
                          <thead>
                            <tr class="table-info" style="font-size:20px">
                              <th>PACIENTE</th>
                              <th>AREA</th>
                              <th>OBSERVACI&Oacute;N</th>
                              <th>ULTIMA ACTUALIZACI&Oacute;N</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            foreach ($opeExpediente as $key => $expediente)
                            {
                              ?>
                                <tr style="font-size:19px">
                                  <td><?php echo $expediente->cat_persona->nombre_completo?></td>
                                  <td style="font-size:18px">
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            echo ($seguimiento->has('cat_area')) ? $seguimiento->cat_area->name : " SIN ESPECIFICAR";
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          echo $expediente->cat_area->name;
                                        }
                                      ?>
                                  </td>
                                  <td>
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            // pr($seguimiento);
                                            echo $seguimiento->observacion;
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          //echo ($expediente->has('cat_area')) ? $expediente->cat_area->name : " SIN ESPECIFICAR";
                                        }
                                      ?>
                                  </td>
                                  <td style="font-size:15px">
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            echo $dias[date('w',strtotime($seguimiento->created))].' '.date('d',strtotime($seguimiento->created)).' DE '.$Meses[date('m',strtotime($seguimiento->created))].' '.date('Y',strtotime($seguimiento->created)). '<br>'.date('H:i a',strtotime($seguimiento->created));
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          echo $dias[date('w',strtotime($expediente->created))].' '.date('d',strtotime($expediente->created)).' DE '.$Meses[date('m',strtotime($expediente->created))].' '.date('Y',strtotime($expediente->created)). '<br>'.date('H:i a',strtotime($expediente->created));
                                          //echo date_format($expediente->created, 'd-m-Y H:i a');;
                                        }
                                      ?>
                                  </td>
                                </tr>
                              <?php
                            }
                            ?>
                          </tbody>
                      </table>

                    </div>
                  </div>
                </div>
            <?php
             }
           ?>
         </div>
         <a class="carousel-control-prev" href="#Banner" role="button" data-slide="prev">
           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
           <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#Banner" role="button" data-slide="next">
           <span class="carousel-control-next-icon" aria-hidden="true"></span>
           <span class="sr-only">Next</span>
         </a>
       </div>
   </div>
 </div>


 <script type="text/javascript">
 $(document).ready(function()
    {

      <?php
          // echo $this->Ajax->RemoteFunction([
          //                                   'type'=>'POST',
          //                                   'data'=>"{unidad_id:'ae91a2d6-7faf-461e-92bd-5b6729d341c0'}",
          //                                   'url'=>['action'=>'getExpediente'],
          //                                   'update'=>'DivExpediente',
          //                                   'before'=>"\$('#DivExpediente').removeClass('animated fadeInDown');",
          //                                   'complete'=>"\$('#DivExpediente').addClass('animated fadeInDown');"
          //                                   ]);
      ?>
    });

    // setInterval(function()
    // {
      <?php
          // echo $this->Ajax->RemoteFunction([
          //                                   'type'=>'POST',
          //                                   'data'=>"{unidad_id:'ae91a2d6-7faf-461e-92bd-5b6729d341c0'}",
          //                                   'url'=>['action'=>'getExpediente'],
          //                                   'update'=>'DivExpediente',
          //                                   'before'=>"\$('#DivExpediente').removeClass('animated fadeInDown');",
          //                                   'complete'=>"\$('#DivExpediente').addClass('animated fadeInDown');"
          //                                   ]);
      ?>
    // },600000); //10min
 </script>
