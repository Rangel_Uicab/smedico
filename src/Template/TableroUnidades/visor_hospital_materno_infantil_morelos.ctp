<script type="text/javascript">
var statSend = false;
function checkSubmit()
{
    if (!statSend)
    {
        statSend = true;
        document.getElementById('btnGuardar').disabled = true;
        return true;
    }
    else
    {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
</script>
<?php
$this->loadHelper('Form', ['templates' => 'app_form']);
?>

<?php echo $this->Html->css(
                            [
                             'animate'
                            ]
                          )
?>
<div class="row">
	<div class="col-md-12">
          <div class="col-md-12" style="text-align: center;">
          <h1 class="text-info">HOSPITAL MATERNO INFANTIL MORELOS</h1>
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo2.jpg"
              title="Remark">
            <img class="img-fluid" src="<?php echo $this->request->webroot; ?>img/logo.png"
              title="Remark">
          </div>
          <!-- <div class="col-md-12" style="text-align: center;">
            <h2>BIENVENIDO(A)</h2>
          </div> -->
	</div>
 </div>
 <div class="row">
   <div class="col-md-12 animated fadeInDown" id="DivExpediente"></div>
 </div>


 <script type="text/javascript">
 $(document).ready(function()
    {
      <?php
          echo $this->Ajax->RemoteFunction([
                                            'type'=>'POST',
                                            'data'=>"{unidad_id:'91067801-5a19-42e3-b9d0-80f57aef0dc0'}",
                                            'url'=>['action'=>'getExpediente'],
                                            'update'=>'DivExpediente',
                                            'before'=>"\$('#DivExpediente').removeClass('animated fadeInDown');",
                                            'complete'=>"\$('#DivExpediente').addClass('animated fadeInDown');"
                                            ]);
      ?>

    // var myInterval = false;
    // myInterval = setInterval(function ()
    // {
    //     var iScroll = $(window).scrollTop();
    //     if (iScroll + $(window).height() == $(document).height())
    //     {
    //         clearInterval(myInterval);
    //     }
    //     else
    //     {
    //         iScroll = iScroll + 200;
    //         $('html, body').animate({
    //                                     scrollTop: iScroll
    //                                 }, 3000);
    //     }
    // }, 2000);

    });

    setInterval(function()
    {
      <?php
          echo $this->Ajax->RemoteFunction([
                                            'type'=>'POST',
                                            'data'=>"{unidad_id:'91067801-5a19-42e3-b9d0-80f57aef0dc0'}",
                                            'url'=>['action'=>'getExpediente'],
                                            'update'=>'DivExpediente',
                                            'before'=>"\$('#DivExpediente').removeClass('animated fadeInDown');",
                                            'complete'=>"\$('#DivExpediente').addClass('animated fadeInDown');"
                                            ]);
      ?>
    },600000); //10min
 </script>
