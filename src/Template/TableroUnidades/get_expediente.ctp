<div class="col-md-12">
    <?php
    // pr($opeExpediente);
    if(!empty($opeExpediente->toArray()))
    {
       $dias = array(
                       1 => "LUNES",
                       2 => "MARTES",
                       3 => "MIERCOLES",
                       4 => "JUEVES",
                       5 => "VIERNES",
                       6 => "SABADO",
                       7 => "DOMINGO",
                     );
       $Meses = array(
                      '01'=>'ENERO',
                      '02'=>'FEBRERO',
                      '03'=>'MARZO',
                      '04'=>'ABRIL',
                      '05'=>'MAYO',
                      '06'=>'JUNIO',
                      '07'=>'JULIO',
                      '08'=>'AGOSTO',
                      '09'=>'SEPTIEMBRE',
                      '10'=>'OCTUBRE',
                      '11'=>'NOVIEMBRE',
                      '12'=>'DICIEMBRE',
                     );
    ?>
     <div class="masonry-item col-md-12">
        <div class="bd bgc-white">
            <div class="layers">
                <div class="layer w-100 p-20 bgc-light-blue-500 c-white p-20" style="text-align:center;">
                    <h4 class="lh-1">VISOR DE SEGUIMIENTO DE PACIENTES</h4>
                </div>
                <div class="layer w-100">
                    <div class="table-responsive p-20">
                      <table class="table table-sm table-striped table-bordered">
                          <thead>
                            <tr style="font-size:20px">
                              <th>PACIENTE</th>
                              <th>AREA</th>
                              <th>OBSERVACI&Oacute;N</th>
                              <th>ULTIMA ACTUALIZACI&Oacute;N</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            foreach ($opeExpediente as $key => $expediente)
                            {
                              ?>
                                <tr style="font-size:19px">
                                  <td><?php echo $expediente->cat_persona->nombre_completo?></td>
                                  <td style="font-size:18px">
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            echo ($seguimiento->has('cat_area')) ? $seguimiento->cat_area->name : " SIN ESPECIFICAR";
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          echo $expediente->cat_area->name;
                                        }
                                      ?>
                                  </td>
                                  <td>
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            // pr($seguimiento);
                                            echo $seguimiento->observacion;
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          //echo ($expediente->has('cat_area')) ? $expediente->cat_area->name : " SIN ESPECIFICAR";
                                        }
                                      ?>
                                  </td>
                                  <td style="font-size:15px">
                                      <?php
                                        if(!empty($expediente->ope_seguimientos_expedientes))
                                        {
                                          foreach ($expediente->ope_seguimientos_expedientes as $key => $seguimiento)
                                          {
                                            echo $dias[date('w',strtotime($seguimiento->created))].' '.date('d',strtotime($seguimiento->created)).' DE '.$Meses[date('m',strtotime($seguimiento->created))].' '.date('Y',strtotime($seguimiento->created)). '<br>'.date('H:i a',strtotime($seguimiento->created));
                                            break 1;
                                          }
                                        }
                                        else
                                        {
                                          echo $dias[date('w',strtotime($expediente->created))].' '.date('d',strtotime($expediente->created)).' DE '.$Meses[date('m',strtotime($expediente->created))].' '.date('Y',strtotime($expediente->created)). '<br>'.date('H:i a',strtotime($expediente->created));
                                          //echo date_format($expediente->created, 'd-m-Y H:i a');;
                                        }
                                      ?>
                                  </td>
                                </tr>
                              <?php
                            }
                            ?>
                          </tbody>
                      </table>
                    </div>
                </div>
            </div>
            <div class="ta-c bdT w-100 p-20">
                <!-- <a href="#"><i class="ti-info-alt"></i> VER HORARIOS DE VISITA</a> -->
            </div>
        </div>
     </div>
    <?php
    }
    else
    {
        ?>
        <div class="alert alert-danger" role="alert" style="text-align:center;">
          <h3>SIN PACIENTES REGISTRADOS!</h3>
        </div>
        <?php
    }
    ?>
</div>

<script type="text/javascript">
 $(document).ready(function() {

});
</script>
