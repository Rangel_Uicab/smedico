<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Core\App;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * CatPersonas Controller
 *
 * @property \App\Model\Table\CatPersonasTable $CatPersonas
 *
 * @method \App\Model\Entity\CatPersona[] paginate($object = null, array $settings = [])
 */
class RecepcionesController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('CatPersonas');
        $this->loadModel('OpeExpedientes');
    }

    public function getData()
    {
         $aColumns = array
                        (
                            'CatPersonas.nombre',
                            'CatPersonas.paterno',
                            'CatPersonas.materno',
                            'CatPersonas.num_expediente',
                            'CatPersonas.num_afiliacion',
                            'CatPersonas.tel_movil',
                            'CatPersonas.tel_casa'
                        );

        $sIndexColumn = "CatPersonas.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        $this->paginate['conditions'][]['CatPersonas.activo']  = 1;

        $UnidadID = $this->request->session()->read('Auth.User.cat_unidade_id');
        if(!empty($UnidadID))
        {
            $this->paginate['conditions'][]['CatPersonas.cat_unidade_id']  = $UnidadID;
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CatPersonas->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = [];
            $catPersonas = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = [];

            $catPersonas = $this->paginate('CatPersonas');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CatPersonas']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CatPersonas']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($catPersonas as $catPersona)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-folder' aria-hidden='true'></i>",array('action'=>'view',$catPersona->id),array('title'=>"Historial",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Html->link("<i class='ti-user' aria-hidden='true'></i>",array('action'=>'edit',$catPersona->id),array('title'=>"Editar",'escape'=>false,'class'=>"btn btn-light"));
            $actions .="</div>";

//                $aaData[$i][] = $catPersona->has('cat_unidade') ? $catPersona->cat_unidade->name : 'S/U';
		        $aaData[$i][] = $catPersona->nombre;
		        $aaData[$i][] = $catPersona->paterno;
		        $aaData[$i][] = $catPersona->materno;
		        $aaData[$i][] = $catPersona->sexo;
		        $aaData[$i][] = $catPersona->num_expediente;
		        $aaData[$i][] = $catPersona->num_afiliacion;
		        $aaData[$i][] = $catPersona->tel_movil;
		        $aaData[$i][] = $catPersona->tel_casa;
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }

    public function getDataExpedientesAbiertos()
    {
         $aColumns = array
                        (
                            'CatAreas.name',
                            'CatPersonas.nombre',
                            'CatPersonas.paterno',
                            'CatPersonas.materno',
                            'CatPersonas.num_expediente',
                            'CatPersonas.num_afiliacion',
                            'OpeExpedientes.motivo',
                            'OpeExpedientes.folio'
                        );

        $sIndexColumn = "OpeExpedientes.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
            $this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
            $this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                    $order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                    $orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        $this->paginate['conditions'][]['OpeExpedientes.activo']  = 1;
        $this->paginate['conditions'][]['OpeExpedientes.en_proceso']  = 1;
        $this->paginate['conditions'][]['OpeExpedientes.cat_unidade_id']  = $this->request->session()->read('Auth.User.cat_unidade_id');

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->OpeExpedientes->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CoUsuarios', 'CatPersonas','CatAreas'];
            $opeExpedientes = $this->paginate($query);
        }
        else
        {
            $this->paginate['contain'] = ['CoUsuarios', 'CatPersonas','CatAreas'];

            $opeExpedientes = $this->paginate('OpeExpedientes');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['OpeExpedientes']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['OpeExpedientes']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($opeExpedientes as $opeExpediente)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-eye' aria-hidden='true'></i>",array('action'=>'ver_seguimiento',$opeExpediente->id),array('title'=>"Ver",'escape'=>false,'class'=>"btn btn-light"));
            $actions .="</div>";

            $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->nombre_completo:'';
            $aaData[$i][] = $opeExpediente->motivo;
            $aaData[$i][] = date('Y-m-d',strtotime($opeExpediente->fecha));
            $aaData[$i][] = date('H:i:s a',strtotime($opeExpediente->hora));
            $aaData[$i][] = $opeExpediente->has('cat_area')? $opeExpediente->cat_area->name:'';
            $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->num_expediente:'';
            $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->num_afiliacion:'';
            $aaData[$i][] = $opeExpediente->folio;
            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }

    public function busquedaExpedientes()
    {
    }

    public function verSeguimiento($id = null)
    {
        $opeExpediente = $this->OpeExpedientes->find()
                                                        ->where(['OpeExpedientes.id'=>$id,'OpeExpedientes.activo'=>1])
                                                        ->contain([
                                                                        'CoUsuarios',
                                                                        'CatPersonas',
                                                                        'CatAtenciones',
                                                                        'CatTiposAltas',
                                                                        'CatUnidades',
                                                                        'CatUnidadesAltas',
                                                                        'CatAreas',
                                                                        'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas','CoUsuarios']
                                                                   ])
                                                        ->contain
                                                                (
                                                                    ['OpeSeguimientosExpedientes'=>
                                                                                                function (\Cake\ORM\Query $q)
                                                                                                        {
                                                                                                            return $q
                                                                                                                    ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                                    ->where(
                                                                                                                                [
                                                                                                                                    'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                                ]
                                                                                                                             );
                                                                                                        }
                                                                    ]
                                                                )
                                                        ->first();
        $this->set('opeExpediente', $opeExpediente);
        $this->set('_serialize', ['opeExpediente']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $opeExpediente = $this->CatPersonas->OpeExpedientes->newEntity();

        $catPersona = $this->CatPersonas->get($id, [
            'contain' => ['CoUsuarios','CatUnidades','OpeExpedientes']
        ]);

        $catAtenciones = TableRegistry::get('CatAtenciones')->find('list', ['conditions' =>['CatAtenciones.activo'=>1] ]);
        $this->set(compact('catAtenciones'));

        $this->set('catPersona', $catPersona);
        $this->set('opeExpediente', $opeExpediente);
        $this->set('_serialize', ['catPersona','opeExpediente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catPersona = $this->CatPersonas->newEntity();

        if ($this->request->is('post'))
        {
            // pr($this->request->data);exit;
            $this->request->data['cat_unidade_id'] = $this->request->session()->read('Auth.User.cat_unidade_id');
            $this->request->data['co_usuario_id'] = $this->request->session()->read('Auth.User.id');
            $this->request->data['activo'] = 1;

            #####   EXPEDIENTE  ######
            $this->request->data['ope_expedientes'][0]['co_usuario_id'] = $this->request->session()->read('Auth.User.id');
            $this->request->data['ope_expedientes'][0]['cat_unidade_id'] = $this->request->session()->read('Auth.User.cat_unidade_id');
            $this->request->data['ope_expedientes'][0]['cat_area_id'] = $this->request->session()->read('Auth.User.cat_area_id');
            $this->request->data['ope_expedientes'][0]['fecha'] = date('Y-m-d');
            $this->request->data['ope_expedientes'][0]['hora'] = date('H:i:s');
            $this->request->data['ope_expedientes'][0]['en_proceso'] = 1;
            $this->request->data['ope_expedientes'][0]['anio'] = date('Y');

            $Folio = $this->code_random();
            $this->request->data['ope_expedientes'][0]['folio'] = $Folio;

            $catPersona = $this->CatPersonas->patchEntity($catPersona, $this->request->getData());

            if ($this->CatPersonas->save($catPersona))
            {
                /*$Expediente = $this->CatPersonas->OpeExpedientes->find()->where([
                                                                                    'OpeExpedientes.fecha'=>$this->request->data['ope_expedientes'][0]['fecha'],
                                                                                    'OpeExpedientes.hora'=>$this->request->data['ope_expedientes'][0]['hora']
                                                                                    ])->first();
                $this->connection = ConnectionManager::get('default');

                $AsignarFolio = $this->connection->execute('CALL asignar_folio("'.$Expediente->id.'","'.$this->request->data['ope_expedientes'][0]['cat_unidade_id'].'")')->fetchAll('assoc');
                $Folio = $AsignarFolio[0]['folio']; */
                $this->Flash->flash('Registro guardado.'.' Su numero de Folio para seguimiento es: '.$Folio , ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'index']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        $catAtenciones = TableRegistry::get('CatAtenciones')->find('list', [
                                                                            'conditions' =>[
                                                                                            'CatAtenciones.id IN
                                                                                            (
                                                                                               SELECT
                                                                                                cat_atencione_id
                                                                                               FROM
                                                                                                cat_unidades_cat_atenciones
                                                                                               WHERE
                                                                                                cat_unidade_id = "'.$this->request->session()->read('Auth.User.cat_unidade_id').'"
                                                                                            )',
                                                                                            'CatAtenciones.activo'=>1
                                                                                            ]
                                                                            ]);

        $this->set(compact('catPersona','catAtenciones'));
        $this->set('_serialize', ['catPersona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catPersona = $this->CatPersonas->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $catPersona = $this->CatPersonas->patchEntity($catPersona, $this->request->getData());
            if ($this->CatPersonas->save($catPersona))
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        $this->set(compact('catPersona'));
        $this->set('_serialize', ['catPersona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catPersona = $this->CatPersonas->get($id);
        $catPersona->activo = 0;
        if ($this->CatPersonas->save($catPersona))
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        }
        else
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addExpediente()
    {
        if ($this->request->is('post'))
        {
            $opeExpediente = $this->CatPersonas->OpeExpedientes->newEntity();

            $this->request->data['co_usuario_id'] = $this->request->session()->read('Auth.User.id');
            $this->request->data['cat_unidade_id'] = $this->request->session()->read('Auth.User.cat_unidade_id');
            $this->request->data['cat_area_id'] = $this->request->session()->read('Auth.User.cat_area_id');
            $this->request->data['fecha'] = date('Y-m-d');
            $this->request->data['hora'] = date('H:i:s');
            $this->request->data['en_proceso'] = 1;
            $this->request->data['anio'] = date('Y');

            $Folio = $this->code_random();
            $this->request->data['folio'] = $Folio;

            $opeExpediente = $this->CatPersonas->OpeExpedientes->patchEntity($opeExpediente, $this->request->getData());
            if ($this->CatPersonas->OpeExpedientes->save($opeExpediente))
            {
//                $this->connection = ConnectionManager::get('default');
//                $AsignarFolio = $this->connection->execute('CALL asignar_folio("'.$opeExpediente->id.'","'.$opeExpediente->cat_unidade_id.'")')->fetchAll('assoc');
//                $Folio = $AsignarFolio[0]['folio'];
                $this->Flash->flash('Registro guardado.'.' Su numero de Folio para seguimiento es: '.$Folio , ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);
                return $this->redirect($this->request->referer());
            }

        }
    }

    public function code_random($long = 5)
    {
        do
        {
            $chars="0123456789abcdefghijklmnopqrstuvwxyz";
            mt_srand((double)microtime()*1000000);
            $i = 0;
            $pass =  "";
            while ($i !=$long)
            {
                $rand =  mt_rand()% strlen($chars);
                $tmp = $chars[$rand];
                $pass = $pass . $tmp;
                $char = str_replace($tmp, "",$chars);
                $i++;
            }
             $VerificaExpediente = $this->OpeExpedientes->find()->where([
                                                                        'OpeExpedientes.folio'=>$pass,
                                                                        'OpeExpedientes.cat_unidade_id'=>$this->request->session()->read('Auth.User.cat_unidade_id'),
                                                                        ])->first();
        } while(!empty($VerificaExpediente));
        return $pass;
    }

}
