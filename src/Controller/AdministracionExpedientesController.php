<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */
class AdministracionExpedientesController extends AppController
{
    public $paginate = array();
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('OpeExpedientes');
    }
    
    public function getData()
    {
         $aColumns = array
                        (
                                'CatAreas.name',
                                'CatPersonas.nombre',
                                'CatPersonas.paterno',
                                'CatPersonas.materno',
                                'CatAtenciones.name',
                                'OpeExpedientes.motivo',
                                'OpeExpedientes.folio'
                        );
        
        $sIndexColumn = "OpeExpedientes.id";
        
        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];        	
        }
        
        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }
        
        $this->paginate['conditions'][]['OpeExpedientes.activo']  = 1;

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->OpeExpedientes->find()->where($this->paginate['conditions']);
            
             $this->paginate['contain'] = ['CatAreas', 'CatPersonas', 'CatAtenciones', 'CatTiposAltas', 'CatUnidades'];
            $opeExpedientes = $this->paginate($query);  
        }
        else
        {
        	$this->paginate['contain'] = ['CatAreas', 'CatPersonas', 'CatAtenciones', 'CatTiposAltas', 'CatUnidades'];
            
            $opeExpedientes = $this->paginate('OpeExpedientes');
        }
        
        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['OpeExpedientes']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['OpeExpedientes']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');
        
        $i = 0;
        foreach($opeExpedientes as $opeExpediente)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-eye' aria-hidden='true'></i>",array('action'=>'view',$opeExpediente->id),array('title'=>"Ver",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Html->link("<i class='ti-pencil' aria-hidden='true'></i>",array('action'=>'edit',$opeExpediente->id),array('title'=>"Editar",'escape'=>false,'class'=>"btn btn-light"));
//                $actions .= $Form->postLink("<i class='ti-trash' aria-hidden='true'></i>", ['action' => 'delete',$opeExpediente->id], ['title'=>"Eliminar",'escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $opeExpediente->id)]);
            $actions .="</div>";
                
                $aaData[$i][] = $opeExpediente->has('cat_unidade') ? $opeExpediente->cat_unidade->name : '';
                $aaData[$i][] = $opeExpediente->has('cat_area') ? $opeExpediente->cat_area->name : '';
                $aaData[$i][] = $opeExpediente->has('cat_persona') ? $opeExpediente->cat_persona->nombre_completo : '';
                $aaData[$i][] = $opeExpediente->has('cat_atencione') ? $opeExpediente->cat_atencione->name : '';
		        $aaData[$i][] = $opeExpediente->motivo;
		        $aaData[$i][] = date_format($opeExpediente->fecha,'d-m-Y'). ' '.date_format($opeExpediente->hora,'H:i a');
                $aaData[$i][] = $opeExpediente->has('cat_tipos_alta_id') ? '<b>DE ALTA </b>' : 'EN PROCESO';
                $aaData[$i][] = $opeExpediente->folio;
//		        $aaData[$i][] = $opeExpediente->has('cat_tipos_alta_id') ? '<b>Tipo: </b>'.$opeExpediente->cat_tipos_alta->name.'<br><b>Fecha: </b>'.date_format($opeExpediente->fecha,'d-m-Y'). ' '.date_format($opeExpediente->hora,'H:i a').'<br><b>Observaci&oacute;n: </b>'.$opeExpediente->observacion_alta : 'EN PROCESO';
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Ope Expediente id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $opeExpediente = $this->OpeExpedientes->get($id, 
                                                        [
                                                            'contain' => [
                                                                            'CoUsuarios', 
                                                                            'CatPersonas', 
                                                                            'CatAtenciones', 
                                                                            'CatTiposAltas', 
                                                                            'CatUnidades', 
                                                                            'OpeSeguimientosExpedientes'=>[
                                                                                                            'CoUsuarios',
                                                                                                            'CatEstatus',
                                                                                                            'CatAreas',
                                                                                                            'sort'=>['OpeSeguimientosExpedientes.created'=>'DESC'],
                                                                                                            'conditions'=>['OpeSeguimientosExpedientes.activo' =>1]
                                                                                                            ]
                                                                            ]
                                                        ]);

        $this->set('opeExpediente', $opeExpediente);
        $this->set('_serialize', ['opeExpediente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ope Expediente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $opeExpediente = $this->OpeExpedientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $opeExpediente = $this->OpeExpedientes->patchEntity($opeExpediente, $this->request->getData());
            if ($this->OpeExpedientes->save($opeExpediente)) 
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $catUnidades = $this->OpeExpedientes->CatUnidades->find('list');
        $catAreas = $this->OpeExpedientes->CatAreas->find('list', ['conditions' =>['CatAreas.cat_unidade_id'=>$this->request->session()->read('Auth.User.cat_unidade_id')]]);
        $catAtenciones = $this->OpeExpedientes->CatAtenciones->find('list', [
                                                                            'conditions' =>[
                                                                                            'CatAtenciones.id IN 
                                                                                            (
                                                                                               SELECT
                                                                                                cat_atencione_id
                                                                                               FROM
                                                                                                cat_unidades_cat_atenciones
                                                                                               WHERE
                                                                                                cat_unidade_id = "'.$this->request->session()->read('Auth.User.cat_unidade_id').'" 
                                                                                            )',
                                                                                            'CatAtenciones.activo'=>1
                                                                                            ] 
                                                                            ]);
        $catTiposAltas = $this->OpeExpedientes->CatTiposAltas->find('list', []);
        $this->set(compact('opeExpediente', 'catAreas', 'catAtenciones', 'catTiposAltas','catUnidades'));
        $this->set('_serialize', ['opeExpediente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ope Expediente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $opeExpediente = $this->OpeExpedientes->get($id);
        $opeExpediente->activo = 0;
        if ($this->OpeExpedientes->save($opeExpediente)) 
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        } 
        else 
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
