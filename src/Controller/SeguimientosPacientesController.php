<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */
class SeguimientosPacientesController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('OpeExpedientes');
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if($this->request->is('post'))
        {
            $this->request->session()->write('FolioExpediente',$this->request->data);    
        }
        if($this->request->session()->check('FolioExpediente'))
        {
            $Folio = $this->request->session()->read('FolioExpediente');    
            $this->request->data = $Folio;    
            $opeExpediente = $this->OpeExpedientes->find()
                                                    ->where(
                                                            [
                                                                'OpeExpedientes.activo'=>1,
                                                                'OpeExpedientes.folio'=>$Folio['folio'],
                                                                'OpeExpedientes.cat_unidade_id'=>$this->request->session()->read('Auth.User.cat_unidade_id')
                                                            ])
                                                    ->contain([
                                                                    'CoUsuarios', 
                                                                    'CatPersonas', 
                                                                    'CatAtenciones', 
                                                                    'CatTiposAltas', 
                                                                    'CatUnidades', 
                                                                    'CatUnidadesAltas', 
                                                                    'CatAreas',
                                                                    'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas','CoUsuarios']
                                                               ])
                                                        ->contain
                                                                (
                                                                    ['OpeSeguimientosExpedientes'=>
                                                                                                function (\Cake\ORM\Query $q) 
                                                                                                        {
                                                                                                            return $q
                                                                                                                    ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                                    ->where(
                                                                                                                                [
                                                                                                                                    'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                                ]
                                                                                                                             );
                                                                                                        }
                                                                    ]
                                                                )
                                                    ->first();
//            pr($opeExpediente);exit;
            $this->set(compact('opeExpediente'));
        }    
        
    }

    public function deleteFiltro()
    {
        if($this->request->session()->check('FolioExpediente'))
        {
            $this->request->session()->delete('FolioExpediente');  
        }
        return $this->redirect($this->request->referer());
    }
    
}
