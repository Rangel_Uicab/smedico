<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */

 ## VISTA PUBLICA DE CONSULTA
class ReportesController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('CatPersonas');
        $this->Auth->allow();
    }

    public function formatoPaciente($id = null)
    {
      $catPersona = $this->CatPersonas->get($id, [
          'contain' => []
      ]);
        $this->set(compact('catPersona'));

    }

}
