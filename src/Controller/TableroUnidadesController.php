<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */

 ## VISTA PUBLICA DE CONSULTA
class TableroUnidadesController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('OpeExpedientes');
        $this->Auth->allow();
    }

    public $dias = array(
                    1 => "LUNES",
                    2 => "MARTES",
                    3 => "MIERCOLES",
                    4 => "JUEVES",
                    5 => "VIERNES",
                    6 => "SABADO",
                    7 => "DOMINGO",
                  );
    public $Meses = array(
                   '01'=>'ENERO',
                   '02'=>'FEBRERO',
                   '03'=>'MARZO',
                   '04'=>'ABRIL',
                   '05'=>'MAYO',
                   '06'=>'JUNIO',
                   '07'=>'JULIO',
                   '08'=>'AGOSTO',
                   '09'=>'SEPTIEMBRE',
                   '10'=>'OCTUBRE',
                   '11'=>'NOVIEMBRE',
                   '12'=>'DICIEMBRE',
                  );
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function visorHospitalMaternoInfantilMorelos()
    {
      $this->viewBuilder()->setLayout('tablero_unidades');
    }
    public function visorHospitalPlayaCarmen()
    {
      $this->viewBuilder()->setLayout('visor');

      $totalPacientesPorPagina = 3;
      $opeExpediente = $this->OpeExpedientes->find()
                                              ->where(
                                                      [
                                                          'OpeExpedientes.activo'=>1,
                                                          'OpeExpedientes.privado'=>0,
                                                          'OpeExpedientes.en_proceso'=>1,
                                                          'OpeExpedientes.cat_unidade_id'=>"ae91a2d6-7faf-461e-92bd-5b6729d341c0"
                                                      ])
                                              ->contain([
                                                              'CatPersonas',
                                                              'CatAtenciones',
                                                              'CatTiposAltas',
                                                              'CatUnidades',
                                                              'CatUnidadesAltas',
                                                              'CatAreas',
                                                              'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas']
                                                         ])
                                                  ->contain
                                                          (
                                                              ['OpeSeguimientosExpedientes'=>
                                                                                          function (\Cake\ORM\Query $q)
                                                                                                  {
                                                                                                      return $q
                                                                                                              ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                              ->where(
                                                                                                                          [
                                                                                                                              'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                          ]
                                                                                                                       );
                                                                                                              //->limit(1);
                                                                                                  }
                                                              ]
                                                          )
                                              ->all();
        // pr($opeExpediente->toArray());
        $totalRegistros = count($opeExpediente->toArray());
        $tablasPre =  $totalRegistros / $totalPacientesPorPagina;
        $tablas = ceil($tablasPre);

        // pr($totalRegistros);
        // pr($tablasPre);
        // pr($tablas);exit;
        $this->set(compact('opeExpediente','tablas','dias','Meses'));

    }

    public function getExpediente()
    {
            $UnidadId = $this->request->data['unidad_id'];

            $opeExpediente = $this->OpeExpedientes->find()
                                                    ->where(
                                                            [
                                                                'OpeExpedientes.activo'=>1,
                                                                'OpeExpedientes.en_proceso'=>1,
                                                                'OpeExpedientes.cat_unidade_id'=>$UnidadId
                                                            ])
                                                    ->contain([
                                                                    'CatPersonas',
                                                                    'CatAtenciones',
                                                                    'CatTiposAltas',
                                                                    'CatUnidades',
                                                                    'CatUnidadesAltas',
                                                                    'CatAreas',
                                                                    'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas']
                                                               ])
                                                        ->contain
                                                                (
                                                                    ['OpeSeguimientosExpedientes'=>
                                                                                                function (\Cake\ORM\Query $q)
                                                                                                        {
                                                                                                            return $q
                                                                                                                    ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                                    ->where(
                                                                                                                                [
                                                                                                                                    'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                                ]
                                                                                                                             );
                                                                                                                    //->limit(1);
                                                                                                        }
                                                                    ]
                                                                )
                                                    ->all();
            // pr($opeExpediente->toArray());exit;

            $this->set(compact('opeExpediente'));

    }

    public function deleteFiltro()
    {
        if($this->request->session()->check('FolioExpediente'))
        {
            $this->request->session()->delete('FolioExpediente');
        }
        return $this->redirect($this->request->referer());
    }

}
