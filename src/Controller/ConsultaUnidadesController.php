<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */

 ## VISTA PUBLICA DE CONSULTA
class ConsultaUnidadesController extends AppController
{
    public $paginate = array();

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('OpeExpedientes');
        $this->Auth->allow();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function visorHospitalMaternoInfantilMorelos()
    {
      $this->viewBuilder()->setLayout('visor');
    }
    public function visorHospitalPlayaCarmen()
    {
      $this->viewBuilder()->setLayout('visor');
    }

    public function getExpediente()
    {
            $Folio = $this->request->data['id'];
            $UnidadId = $this->request->data['unidad_id'];

            $opeExpediente = $this->OpeExpedientes->find()
                                                    ->where(
                                                            [
                                                                'OpeExpedientes.activo'=>1,
                                                                'OpeExpedientes.folio'=>$Folio,
                                                                'OpeExpedientes.cat_unidade_id'=>$UnidadId
                                                            ])
                                                    ->contain([
                                                                    'CatPersonas',
                                                                    'CatAtenciones',
                                                                    'CatTiposAltas',
                                                                    'CatUnidades',
                                                                    'CatUnidadesAltas',
                                                                    'CatAreas',
                                                                    'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas']
                                                               ])
                                                        ->contain
                                                                (
                                                                    ['OpeSeguimientosExpedientes'=>
                                                                                                function (\Cake\ORM\Query $q)
                                                                                                        {
                                                                                                            return $q
                                                                                                                    ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                                    ->where(
                                                                                                                                [
                                                                                                                                    'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                                ]
                                                                                                                             );
                                                                                                        }
                                                                    ]
                                                                )
                                                    ->first();
            // pr($opeExpediente->toArray());exit;

            $this->set(compact('opeExpediente'));

    }

    public function deleteFiltro()
    {
        if($this->request->session()->check('FolioExpediente'))
        {
            $this->request->session()->delete('FolioExpediente');
        }
        return $this->redirect($this->request->referer());
    }

}
