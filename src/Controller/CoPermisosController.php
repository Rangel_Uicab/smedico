<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * CoPermisos Controller
 *
 * @property \App\Model\Table\CoPermisosTable $CoPermisos
 *
 * @method \App\Model\Entity\CoPermiso[] paginate($object = null, array $settings = [])
 */
class CoPermisosController extends AppController
{
    public $paginate = array();

    
    public function getData()
    {
         $aColumns = array
                        (
                            'CoPermisos.name',
                            'CoPermisos.descripcion',
                            'CoPermisos.controller',
                            'CoPermisos.action'
                        );
        
        $sIndexColumn = "CoPermisos.id";
        
        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];        	
        }
        
        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }
        
        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CoPermisos->find()->where($this->paginate['conditions']);
            
             $this->paginate['contain'] = [];
            $coPermisos = $this->paginate($query);  
        }
        else
        {
        	$this->paginate['contain'] = [];
            
            $coPermisos = $this->paginate('CoPermisos');
        }
        
        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CoPermisos']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CoPermisos']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');
        
        $i = 0;
        foreach($coPermisos as $coPermiso)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-eye' aria-hidden='true'></i>",array('action'=>'view',$coPermiso->id),array('title'=>"Ver",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Html->link("<i class='ti-pencil' aria-hidden='true'></i>",array('action'=>'edit',$coPermiso->id),array('title'=>"Editar",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Form->postLink("<i class='ti-trash' aria-hidden='true'></i>", ['action' => 'delete',$coPermiso->id], ['title'=>"Eliminar",'escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $coPermiso->id)]);
            $actions .="</div>";
            	$aaData[$i][] = $coPermiso->id;
		        $aaData[$i][] = $coPermiso->name;
		        $aaData[$i][] = $coPermiso->descripcion;
		        $aaData[$i][] = $coPermiso->controller;
		        $aaData[$i][] = $coPermiso->action;
		        $aaData[$i][] = $coPermiso->activo;
		        $aaData[$i][] = $coPermiso->created;
		        $aaData[$i][] = $coPermiso->modified;
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Co Permiso id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $coPermiso = $this->CoPermisos->get($id, [
            'contain' => ['CoGrupos']
        ]);

        $this->set('coPermiso', $coPermiso);
        $this->set('_serialize', ['coPermiso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $coPermiso = $this->CoPermisos->newEntity();
        if ($this->request->is('post')) {
            $coPermiso = $this->CoPermisos->patchEntity($coPermiso, $this->request->getData());
            if ($this->CoPermisos->save($coPermiso)) 
            {
               
                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'index']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);
		
        }
        $coGrupos = $this->CoPermisos->CoGrupos->find('list', ['limit' => 200]);
        $this->set(compact('coPermiso', 'coGrupos'));
        $this->set('_serialize', ['coPermiso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Co Permiso id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coPermiso = $this->CoPermisos->get($id, [
            'contain' => ['CoGrupos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coPermiso = $this->CoPermisos->patchEntity($coPermiso, $this->request->getData());
            if ($this->CoPermisos->save($coPermiso)) 
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $coGrupos = $this->CoPermisos->CoGrupos->find('list', ['limit' => 200]);
        $this->set(compact('coPermiso', 'coGrupos'));
        $this->set('_serialize', ['coPermiso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Co Permiso id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coPermiso = $this->CoPermisos->get($id);
        if ($this->CoPermisos->delete($coPermiso)) 
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        } 
        else 
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
