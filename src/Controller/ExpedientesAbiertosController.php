<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * OpeExpedientes Controller
 *
 * @property \App\Model\Table\OpeExpedientesTable $OpeExpedientes
 *
 * @method \App\Model\Entity\OpeExpediente[] paginate($object = null, array $settings = [])
 */
class ExpedientesAbiertosController extends AppController
{
    public $paginate = array();
    
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('OpeExpedientes');
        $this->loadModel('OpeSeguimientosExpedientes');
    }
    
    public function getData()
    {
         $aColumns = array
                        (
                            'CatAreas.name',
                            'CatPersonas.nombre',
                            'CatPersonas.paterno',
                            'CatPersonas.materno',
                            'CatPersonas.num_expediente',
                            'CatPersonas.num_afiliacion',
                            'OpeExpedientes.motivo',
                            'OpeExpedientes.folio'
                        );
        
        $sIndexColumn = "OpeExpedientes.id";
        
        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];        	
        }
        
        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }
        
        $this->paginate['conditions'][]['OpeExpedientes.activo']  = 1;
        $this->paginate['conditions'][]['OpeExpedientes.en_proceso']  = 1;
        $this->paginate['conditions'][]['OpeExpedientes.cat_unidade_id']  = $this->request->session()->read('Auth.User.cat_unidade_id');
        
        if(!empty($this->paginate['conditions']))
        {
            $query = $this->OpeExpedientes->find()->where($this->paginate['conditions']);
            
             $this->paginate['contain'] = ['CoUsuarios', 'CatPersonas','CatAreas'];
            $opeExpedientes = $this->paginate($query);  
        }
        else
        {
        	$this->paginate['contain'] = ['CoUsuarios', 'CatPersonas','CatAreas'];
            
            $opeExpedientes = $this->paginate('OpeExpedientes');
        }
        
        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['OpeExpedientes']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['OpeExpedientes']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');
        
        $i = 0;
        foreach($opeExpedientes as $opeExpediente)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-eye' aria-hidden='true'></i>",array('action'=>'view',$opeExpediente->id),array('title'=>"Ver",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Html->link("<i class='ti-pencil' aria-hidden='true'></i>",array('action'=>'edit',$opeExpediente->id),array('title'=>"Editar",'escape'=>false,'class'=>"btn btn-light"));
            $actions .="</div>";

		    $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->nombre_completo:'';
		    $aaData[$i][] = $opeExpediente->motivo;
            $aaData[$i][] = date('Y-m-d',strtotime($opeExpediente->fecha));
            $aaData[$i][] = date('H:i:s a',strtotime($opeExpediente->hora));
            $aaData[$i][] = $opeExpediente->has('cat_area')? $opeExpediente->cat_area->name:'';
            $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->num_expediente:'';
            $aaData[$i][] = $opeExpediente->has('cat_persona')? $opeExpediente->cat_persona->num_afiliacion:'';
            $aaData[$i][] = $opeExpediente->folio;
	        $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }
    
    public function edit($id = null)
    {
        $opeExpediente = $this->OpeExpedientes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $opeExpediente = $this->OpeExpedientes->patchEntity($opeExpediente, $this->request->getData());
            if ($this->OpeExpedientes->save($opeExpediente)) 
            {
                $this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
                   $this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }
        $coUsuarios = $this->OpeExpedientes->CoUsuarios->find('list', ['limit' => 200]);
        $catPersonas = $this->OpeExpedientes->CatPersonas->find('list', ['conditions' =>['CatPersonas.cat_unidade_id'=>$this->request->session()->read('Auth.User.cat_unidade_id')]]);
        $catAtenciones = $this->OpeExpedientes->CatAtenciones->find('list', ['limit' => 200]);
        $catTiposAltas = $this->OpeExpedientes->CatTiposAltas->find('list', ['limit' => 200]);
        $catUnidades = $this->OpeExpedientes->CatUnidades->find('list', ['limit' => 200]);
        $this->set(compact('opeExpediente', 'coUsuarios', 'catPersonas', 'catAtenciones', 'catTiposAltas', 'catUnidades'));
        $this->set('_serialize', ['opeExpediente']);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Ope Expediente id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $opeExpediente = $this->OpeExpedientes->find()
                                                        ->where(['OpeExpedientes.id'=>$id])
                                                        ->contain([
                                                                        'CoUsuarios', 
                                                                        'CatPersonas', 
                                                                        'CatAtenciones', 
                                                                        'CatTiposAltas', 
                                                                        'CatUnidades', 
                                                                        'CatUnidadesAltas', 
                                                                        'CatAreas',
                                                                        'OpeSeguimientosExpedientes'=>['CatEstatus','CatAreas','CoUsuarios']
                                                                   ])
                                                        ->contain
                                                                (
                                                                    ['OpeSeguimientosExpedientes'=>
                                                                                                function (\Cake\ORM\Query $q) 
                                                                                                        {
                                                                                                            return $q
                                                                                                                    ->order(['OpeSeguimientosExpedientes.created'=>'DESC'])
                                                                                                                    ->where(
                                                                                                                                [
                                                                                                                                    'OpeSeguimientosExpedientes.activo' =>1
                                                                                                                                ]
                                                                                                                             );
                                                                                                        }
                                                                    ]
                                                                )
                                                        ->first();

//        pr($opeExpediente->toArray());exit;
        $opeSeguimientosExpediente = $this->OpeExpedientes->OpeSeguimientosExpedientes->newEntity();
        
        $catAreas = $this->OpeExpedientes->OpeSeguimientosExpedientes->CatAreas->find('list',['conditions'=>['CatAreas.cat_unidade_id'=>$this->request->session()->read('Auth.User.cat_unidade_id'),'CatAreas.activo'=>1]]);
        $catEstatus = $this->OpeExpedientes->OpeSeguimientosExpedientes->CatEstatus->find('list');
        $catTiposAltas = $this->OpeExpedientes->CatTiposAltas->find('list');
        
        $this->set(compact('catEstatus','catAreas','catTiposAltas'));
        $this->set('opeExpediente', $opeExpediente);
        $this->set('opeSeguimientosExpediente', $opeSeguimientosExpediente);
        $this->set('_serialize', ['opeExpediente','opeSeguimientosExpediente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    /**
     * Edit method
     *
     * @param string|null $id Ope Expediente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function altaPaciente($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $opeExpediente = $this->OpeExpedientes->get($id, ['contain' => []]);
            $this->request->data['fecha_alta'] = date('Y-m-d');
            $this->request->data['hora_alta'] = date('H:i:s');
            $this->request->data['en_proceso'] = 0;
            $opeExpediente = $this->OpeExpedientes->patchEntity($opeExpediente, $this->request->getData());
            if ($this->OpeExpedientes->save($opeExpediente)) 
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
            }
            else
            {
                $this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
            }
        }
        return $this->redirect($this->request->referer());
    }
    
    public function getTrasladoUnidad()
    {
        if($this->request->is('post'))
        {
            $catTiposAltas = $this->OpeExpedientes->CatTiposAltas->get($this->request->data['id']);
            $catUnidades = $this->OpeExpedientes->CatUnidades->find('list',['conditions'=>['CatUnidades.activo'=>1]]);

            $this->set(compact('catUnidades','catTiposAltas'));
        }
    }
    
    ##### FUNCIONES DE SEGUIMIENTOS EXPEDIENTES (DETALLES) #####

    public function addSeguimiento()
    {
        $opeSeguimientosExpediente = $this->OpeSeguimientosExpedientes->newEntity();
        if ($this->request->is('post')) 
        {
            $this->request->data['co_usuario_id'] = $this->request->session()->read('Auth.User.id');
            $this->request->data['activo'] = 1;

            $opeSeguimientosExpediente = $this->OpeSeguimientosExpedientes->patchEntity($opeSeguimientosExpediente, $this->request->getData());
            if ($this->OpeSeguimientosExpedientes->save($opeSeguimientosExpediente)) 
            {
                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);
            }
            else
            {
                $this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);
            }
        }
        return $this->redirect($this->request->referer());
    }

    public function editSeguimiento($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $opeSeguimientosExpediente = $this->OpeSeguimientosExpedientes->get($id,['contain' => []]);

            $opeSeguimientosExpediente = $this->OpeSeguimientosExpedientes->patchEntity($opeSeguimientosExpediente, $this->request->getData());
            if ($this->OpeSeguimientosExpedientes->save($opeSeguimientosExpediente)) 
            {
                $this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
            }
            else
            {
                $this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
            }
        }
        return $this->redirect($this->request->referer());
    }

    public function deleteSeguimiento($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $opeSeguimientosExpediente = $this->OpeSeguimientosExpedientes->get($id);
        $opeSeguimientosExpediente->activo = 0;
        if ($this->OpeSeguimientosExpedientes->save($opeSeguimientosExpediente)) 
        {
            $this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        } 
        else 
        {
            $this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect($this->request->referer());
    }
    ##### FUNCIONES DE SEGUIMIENTOS EXPEDIENTES (DETALLES) #####
    
}
