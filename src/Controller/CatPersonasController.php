<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\App;

/**
 * CatPersonas Controller
 *
 * @property \App\Model\Table\CatPersonasTable $CatPersonas
 *
 * @method \App\Model\Entity\CatPersona[] paginate($object = null, array $settings = [])
 */
class CatPersonasController extends AppController
{
    public $paginate = array();


    public function getData()
    {
         $aColumns = array
                        (
                            'CatPersonas.id',
                                'CatPersonas.co_usuario_id',
                                'CatPersonas.cat_municipio_id',
                                'CatPersonas.cat_localidade_id',
                                'CatPersonas.nombre',
                                'CatPersonas.paterno',
                                'CatPersonas.materno',
                                'CatPersonas.sexo',
                                'CatPersonas.num_expediente',
                                'CatPersonas.num_afiliacion',
                                'CatPersonas.fecha_nacimiento',
                                'CatPersonas.tel_movil',
                                'CatPersonas.tel_casa',
                                'CatPersonas.direccion',
                                'CatPersonas.activo',
                                'CatPersonas.created',
                                'CatPersonas.modified',
                                );

        $sIndexColumn = "CatPersonas.id";

        //Verificamos que nos enviaron la cantidad de registros que se requieren por pagina
        if(isset($this->request->query['iDisplayLength']))
        {
        	$this->paginate['maxLimit'] =$this->request->query['iDisplayLength'];
        	$this->paginate['limit'] =$this->request->query['iDisplayLength'];
        }

        //Verificamos si nos enviaron la pagina que desean visualizar
        if(isset($this->request->query['iDisplayStart']))
        {
            //Se realiza la division para obtener el numero de pagina
            $this->paginate['page'] = ($this->request->query['iDisplayStart']/$this->request->query['iDisplayLength'])+1;
        }
        //Verificamos si se envio algun orden de columna en especifico
        if(isset($this->request->query['iSortCol_0']))
        {
            $orden = [];
            for ( $i=0 ; $i < intval( $this->request->query['iSortingCols'] ) ; $i++ )
            {
                if ( $this->request->query[ 'bSortable_'.intval($this->request->query['iSortCol_'.$i]) ] == "true" )
                {
                    $column = $aColumns[ intval( $this->request->query['iSortCol_'.$i] ) ];
                	$order = ($this->request->query['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                	$orden = [ $column => $order];
                }
            }
            //Si la cadena no esta vacia se la agregamos a las opciones del paginador en la opcion "order"
            if ( !empty($orden))
            {
                $this->paginate['order'] = $orden;
            }
        }
        //Revisamos si se envio el filtro para todos los campos
        $conditions = array();
        if ( isset($this->request->query['sSearch']) && $this->request->query['sSearch'] != "" )
        {
            for ( $i=0 ; $i < count($aColumns) ; $i++ )
            {
                $conditions[][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
            }
            //Si el arreglo de condiciones no esta vacio, lo pasamos a las opciones del Paginador con el operador OR
            if(!empty($conditions))
            {
                $this->paginate['conditions']['OR'] = $conditions;
            }
        }
        //Verificamos si se envio algun filtro de campo especifico
        for ( $i=0 ; $i < count($aColumns) ; $i++ )
        {
            if ( isset($this->request->query['bSearchable_'.$i]) && $this->request->query['bSearchable_'.$i] == "true" && ($this->request->query['sSearch_'.$i] != '' || $this->request->query['sSearch'] != '') )
            {
                if(!empty($this->request->query['sSearch']))
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch'].'%';
                else
                    $this->paginate['conditions']['OR'][][$aColumns[$i].' LIKE']='%'.$this->request->query['sSearch_'.$i].'%';
            }
        }

        if(!empty($this->paginate['conditions']))
        {
            $query = $this->CatPersonas->find()->where($this->paginate['conditions']);

             $this->paginate['contain'] = ['CoUsuarios', 'CatMunicipios', 'CatLocalidades'];
            $catPersonas = $this->paginate($query);
        }
        else
        {
        	$this->paginate['contain'] = ['CoUsuarios', 'CatMunicipios', 'CatLocalidades'];

            $catPersonas = $this->paginate('CatPersonas');
        }

        //Numero total de registros
        $iTotalDisplayRecords = $this->request->params['paging']['CatPersonas']['count'];

        //Numero de registros encontrados
        $iTotalRecords = $this->request->params['paging']['CatPersonas']['current'];

        //sEcho
        $sEcho = intval($this->request->query['sEcho']);

        //Datos para la tabla
        $aaData = array();

        //Cargamos los Helper para armar los links de acciones
        $View = new \App\View\AppView();
        App::classname('Html', 'View/Helper', 'Helper');
        $Html = $View->loadHelper('Html');
        $Form = $View->loadHelper('Form');

        $i = 0;
        foreach($catPersonas as $catPersona)
        {

            $actions = "<div class='btn-group' role='group'>";
                $actions .= $Html->link("<i class='ti-eye' aria-hidden='true'></i>",array('action'=>'view',$catPersona->id),array('title'=>"Ver",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Html->link("<i class='ti-pencil' aria-hidden='true'></i>",array('action'=>'edit',$catPersona->id),array('title'=>"Editar",'escape'=>false,'class'=>"btn btn-light"));
                $actions .= $Form->postLink("<i class='ti-trash' aria-hidden='true'></i>", ['action' => 'delete',$catPersona->id], ['title'=>"Eliminar",'escape'=>false,'class'=>"btn btn-danger",'confirm' => __('Realmente desea eliminar el registro con el Id # {0}?', $catPersona->id)]);
            $actions .="</div>";
            	$aaData[$i][] = $catPersona->id;
		$aaData[$i][] = $catPersona->co_usuario_id;
		$aaData[$i][] = $catPersona->cat_municipio_id;
		$aaData[$i][] = $catPersona->cat_localidade_id;
		$aaData[$i][] = $catPersona->nombre;
		$aaData[$i][] = $catPersona->paterno;
		$aaData[$i][] = $catPersona->materno;
		$aaData[$i][] = $catPersona->sexo;
		$aaData[$i][] = $catPersona->num_expediente;
		$aaData[$i][] = $catPersona->num_afiliacion;
		$aaData[$i][] = $catPersona->fecha_nacimiento;
		$aaData[$i][] = $catPersona->tel_movil;
		$aaData[$i][] = $catPersona->tel_casa;
		$aaData[$i][] = $catPersona->direccion;
		$aaData[$i][] = $catPersona->activo;
		$aaData[$i][] = $catPersona->created;
		$aaData[$i][] = $catPersona->modified;
	            $aaData[$i][] = $actions;
            $i++;
        }
        //Enviamos y serializamos en JSON todas la variables requeridas por el jquery.dataTable
        $this->set(compact('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_serialize',array('sEcho','iTotalRecords','iTotalDisplayRecords','aaData'));
        $this->set('_jsonp',true);
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catPersona = $this->CatPersonas->get($id, [
            'contain' => ['CoUsuarios', 'CatMunicipios', 'CatLocalidades', 'OpeExpedientes']
        ]);

        $this->set('catPersona', $catPersona);
        $this->set('_serialize', ['catPersona']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catPersona = $this->CatPersonas->newEntity();
        if ($this->request->is('post'))
        {
            $catPersona = $this->CatPersonas->patchEntity($catPersona, $this->request->getData());
            if ($this->CatPersonas->save($catPersona))
            {

                $this->Flash->flash('Registro guardado.', ['params'=>['type'=>'info']]);

                return $this->redirect(['action' => 'index']);
            }
			 	$this->Flash->flash('El Registro no pudo ser guardado.', ['params'=>['type'=>'danger']]);

        }
        $coUsuarios = $this->CatPersonas->CoUsuarios->find('list', ['limit' => 200]);
        $catMunicipios = $this->CatPersonas->CatMunicipios->find('list', ['limit' => 200]);
        $catLocalidades = $this->CatPersonas->CatLocalidades->find('list', ['limit' => 200]);
        $this->set(compact('catPersona', 'coUsuarios', 'catMunicipios', 'catLocalidades'));
        $this->set('_serialize', ['catPersona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $catPersona = $this->CatPersonas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catPersona = $this->CatPersonas->patchEntity($catPersona, $this->request->getData());
            if ($this->CatPersonas->save($catPersona))
            {
            	$this->Flash->flash('Registro actualizado correctamente.', ['params'=>['type'=>'info']]);
                return $this->redirect(['action' => 'index']);
            }
           		$this->Flash->flash('El registro no se pudo actualizar correctamente. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
    }
        $coUsuarios = $this->CatPersonas->CoUsuarios->find('list', ['limit' => 200]);
        $catMunicipios = $this->CatPersonas->CatMunicipios->find('list', ['limit' => 200]);
        $catLocalidades = $this->CatPersonas->CatLocalidades->find('list', ['limit' => 200]);
        $this->set(compact('catPersona', 'coUsuarios', 'catMunicipios', 'catLocalidades'));
        $this->set('_serialize', ['catPersona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cat Persona id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catPersona = $this->CatPersonas->get($id);
        if ($this->CatPersonas->delete($catPersona))
        {
        	$this->Flash->flash('Registro eliminado correctamente.', ['params'=>['type'=>'info']]);
        }
        else
        {
			$this->Flash->flash('El registro no pudo ser eliminado. Intentelo nuevamente', ['params'=>['type'=>'danger']]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
