<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OpeExpedientesFixture
 *
 */
class OpeExpedientesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'co_usuario_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_unidade_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_area_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_persona_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_atencione_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_tipos_alta_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_unidade_traslado_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'motivo' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'utf8_spanish_ci', 'comment' => '', 'precision' => null],
        'fecha' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'hora' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'observacion' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_spanish_ci', 'comment' => '', 'precision' => null],
        'observacion_alta' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_spanish_ci', 'comment' => '', 'precision' => null],
        'fecha_alta' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'hora_alta' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_ope_expedientes_cat_personas1_idx' => ['type' => 'index', 'columns' => ['cat_persona_id'], 'length' => []],
            'fk_ope_expediente_cat_tipos_altas1_idx' => ['type' => 'index', 'columns' => ['cat_tipos_alta_id'], 'length' => []],
            'fk_ope_expediente_cat_unidades1_idx' => ['type' => 'index', 'columns' => ['cat_unidade_traslado_id'], 'length' => []],
            'fk_ope_expediente_co_usuarios1_idx' => ['type' => 'index', 'columns' => ['co_usuario_id'], 'length' => []],
            'fk_ope_expediente_cat_atenciones1_idx' => ['type' => 'index', 'columns' => ['cat_atencione_id'], 'length' => []],
            'fk_ope_expedientes_cat_areas1_idx' => ['type' => 'index', 'columns' => ['cat_area_id'], 'length' => []],
            'fk_ope_expedientes_cat_unidades1_idx' => ['type' => 'index', 'columns' => ['cat_unidade_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_ope_expediente_cat_atenciones1' => ['type' => 'foreign', 'columns' => ['cat_atencione_id'], 'references' => ['cat_atenciones', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expediente_cat_tipos_altas1' => ['type' => 'foreign', 'columns' => ['cat_tipos_alta_id'], 'references' => ['cat_tipos_altas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expediente_cat_unidades1' => ['type' => 'foreign', 'columns' => ['cat_unidade_traslado_id'], 'references' => ['cat_unidades', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expediente_co_usuarios1' => ['type' => 'foreign', 'columns' => ['co_usuario_id'], 'references' => ['co_usuarios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expedientes_cat_areas1' => ['type' => 'foreign', 'columns' => ['cat_area_id'], 'references' => ['cat_areas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expedientes_cat_personas1' => ['type' => 'foreign', 'columns' => ['cat_persona_id'], 'references' => ['cat_personas', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ope_expedientes_cat_unidades1' => ['type' => 'foreign', 'columns' => ['cat_unidade_id'], 'references' => ['cat_unidades', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_spanish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 'bef34d3f-a331-41ce-9487-8bbf8f08f322',
            'co_usuario_id' => 'fb643282-b4d3-4596-9a94-2fb55880faaf',
            'cat_unidade_id' => '467697c6-207b-456a-b0fe-4de7c23c97d3',
            'cat_area_id' => 'dec5965e-f38b-4d32-9eea-10d4e6ec534d',
            'cat_persona_id' => '435a8c27-0e6b-41e6-ab52-5b85d0635adc',
            'cat_atencione_id' => 'a97a70c8-1d83-4cee-ae08-9f1af9594703',
            'cat_tipos_alta_id' => '355dde3d-a478-4a4c-86c9-a54edad2f38a',
            'cat_unidade_traslado_id' => '9c89e545-4709-40fd-8ae9-b2c2c8349887',
            'motivo' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'fecha' => '2018-04-17',
            'hora' => '21:15:48',
            'observacion' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'observacion_alta' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'fecha_alta' => '2018-04-17',
            'hora_alta' => '21:15:48',
            'created' => '2018-04-17 21:15:48',
            'modified' => '2018-04-17 21:15:48'
        ],
    ];
}
