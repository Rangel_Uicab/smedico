<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CatUnidadesFixture
 *
 */
class CatUnidadesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cat_tipos_unidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cat_localidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cat_municipio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nombre_corto' => ['type' => 'string', 'length' => 80, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'latitud' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'longitud' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'activo' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_cat_unidades_cat_tipos_unidades_centrales1_idx' => ['type' => 'index', 'columns' => ['cat_tipos_unidade_id'], 'length' => []],
            'fk_cat_unidades_cat_municipios1_idx' => ['type' => 'index', 'columns' => ['cat_municipio_id'], 'length' => []],
            'fk_cat_unidades_cat_localidades1_idx' => ['type' => 'index', 'columns' => ['cat_localidade_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_cat_unidades_cat_localidades1' => ['type' => 'foreign', 'columns' => ['cat_localidade_id'], 'references' => ['cat_localidades', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_cat_unidades_cat_municipios1' => ['type' => 'foreign', 'columns' => ['cat_municipio_id'], 'references' => ['cat_municipios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_cat_unidades_cat_tipos_unidades_centrales1' => ['type' => 'foreign', 'columns' => ['cat_tipos_unidade_id'], 'references' => ['cat_tipos_unidades', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_spanish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => '6932407f-8d43-4333-befc-eca9b8e691e7',
            'cat_tipos_unidade_id' => 1,
            'cat_localidade_id' => 1,
            'cat_municipio_id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'nombre_corto' => 'Lorem ipsum dolor sit amet',
            'latitud' => 'Lorem ipsum dolor sit amet',
            'longitud' => 'Lorem ipsum dolor sit amet',
            'activo' => 1,
            'created' => '2018-04-17 17:33:39',
            'modified' => '2018-04-17 17:33:39'
        ],
    ];
}
