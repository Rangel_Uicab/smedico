<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatAtencionesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatAtencionesTable Test Case
 */
class CatAtencionesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatAtencionesTable
     */
    public $CatAtenciones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_atenciones',
        'app.ope_expedientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatAtenciones') ? [] : ['className' => 'App\Model\Table\CatAtencionesTable'];
        $this->CatAtenciones = TableRegistry::get('CatAtenciones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatAtenciones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
