<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CatCargosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CatCargosTable Test Case
 */
class CatCargosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CatCargosTable
     */
    public $CatCargos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cat_cargos',
        'app.cat_personales'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CatCargos') ? [] : ['className' => 'App\Model\Table\CatCargosTable'];
        $this->CatCargos = TableRegistry::get('CatCargos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CatCargos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
