<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoPermisosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoPermisosTable Test Case
 */
class CoPermisosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoPermisosTable
     */
    public $CoPermisos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.co_permisos',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_grupos_co_permisos',
        'app.co_usuarios',
        'app.co_usuarios_co_grupos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CoPermisos') ? [] : ['className' => 'App\Model\Table\CoPermisosTable'];
        $this->CoPermisos = TableRegistry::get('CoPermisos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CoPermisos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
