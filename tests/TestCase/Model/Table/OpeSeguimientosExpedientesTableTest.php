<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OpeSeguimientosExpedientesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OpeSeguimientosExpedientesTable Test Case
 */
class OpeSeguimientosExpedientesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OpeSeguimientosExpedientesTable
     */
    public $OpeSeguimientosExpedientes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ope_seguimientos_expedientes',
        'app.ope_expedientes',
        'app.co_usuarios',
        'app.co_grupos',
        'app.co_menus',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios_co_grupos',
        'app.cat_personas',
        'app.cat_municipios',
        'app.cat_estados',
        'app.cat_localidades',
        'app.cat_unidades',
        'app.cat_tipos_unidades',
        'app.cat_areas',
        'app.cat_atenciones',
        'app.cat_tipos_altas',
        'app.cat_estatus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OpeSeguimientosExpedientes') ? [] : ['className' => 'App\Model\Table\OpeSeguimientosExpedientesTable'];
        $this->OpeSeguimientosExpedientes = TableRegistry::get('OpeSeguimientosExpedientes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OpeSeguimientosExpedientes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
