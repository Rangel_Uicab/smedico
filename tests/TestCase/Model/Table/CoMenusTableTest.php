<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoMenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoMenusTable Test Case
 */
class CoMenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoMenusTable
     */
    public $CoMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.co_menus',
        'app.co_grupos',
        'app.co_grupos_co_menus',
        'app.co_permisos',
        'app.co_grupos_co_permisos',
        'app.co_usuarios',
        'app.co_usuarios_co_grupos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CoMenus') ? [] : ['className' => 'App\Model\Table\CoMenusTable'];
        $this->CoMenus = TableRegistry::get('CoMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CoMenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
